package view.interfaces;

/**
 * Interface representing a card image
 * 
 * @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */
public interface CardImage {

	public abstract String getFileString();
	
}
