package view;

import java.util.HashMap;
import java.util.Map;

import javafx.scene.image.Image;
import javafx.scene.paint.ImagePattern;

/**
 * Image Pattern Factory class which stores the ImagePattern for each card id.
 * 
 * @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */
public class ImagePatternFactory {

	private static Map<String, ImagePattern> cardImagePatterns;
	private static ImagePattern cardBackImagePattern;
	
	//string array of card image file paths
	private static final String[] CARD_IMAGE_FILE_STRINGS = new String[] { 
		"/tIntersectCard.jpg",
		"/straightCard.jpg",
		"/rightTurnCard.jpg",
		"/leftTurnCard.jpg",
		"/fourWayIntersectCard.jpg",
		"/deadEndCard.jpg",
		"/startCard.jpg",
		"/falseGoalCard.jpg",
		"/trueGoalCard.jpg",
		"/clearPathActionCard.jpg",
		"/goalRevealActionCard.jpg",
		"/landfillActionCard.jpg",
		"/landfillRemovalActionCard.jpg",
		"/logBlockActionCard.jpg",
		"/logBlockRemovalActionCard.jpg",
		"/chippedToothCard.jpg",
		"/hurtPawCard.jpg",
		"/healedToothCard.jpg",
		"/healedPawCard.jpg",
		"/skipTurnCard.jpg",
		"/immunityCard.jpg"
	};
	
	
	/**
	 * @return the ImagePattern for the back of a card
	 */
	public static ImagePattern getCardBackImagePattern(){
		
		if(cardBackImagePattern == null)
			cardBackImagePattern =  new ImagePattern(new CardImageImpl("/cardBack.jpg"));	
		
		return cardBackImagePattern;
	}

	/**
	 * return the ImagePattern for a given card id
	 * 
	 * @param cardType - string id for the card
	 * @return ImagePattern
	 */
	public static ImagePattern getCardImagePattern(String cardType){

		if (cardImagePatterns == null)
			createCardImagePatterns();
		
		return cardImagePatterns.get(cardType);
	}

	/**
	 * Create the ImagePattern for each card and store it with the card type id (sourced from the CardTypeEnum)
	 * in a HashMap
	 */
	private static void createCardImagePatterns(){
	
		cardImagePatterns = new HashMap<String, ImagePattern>();

		for (CardTypeEnum pathCardType: CardTypeEnum.values()){
			Image image = new CardImageImpl(CARD_IMAGE_FILE_STRINGS[pathCardType.ordinal()]);
			cardImagePatterns.put(pathCardType.name(), new ImagePattern(image));
		}
	}
	

}