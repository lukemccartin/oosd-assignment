package view;

import java.util.Observable;
import java.util.Observer;

import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import model.TurnManagerImpl;

/**
 * HBox which contains the timer display
 * 
 *  @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */
public class TimerHBox extends HBox implements Observer {

	private Text turnTimeRemainingText;
	
	public TimerHBox() {
		setupHBox();
		TurnManagerImpl.getSingletonInstance().getTurnTimerService().addObserver(this);
	}
	
	private void setupHBox(){
		turnTimeRemainingText = new Text();
		Label timerLabel = new Label ("Seconds Left This Turn: ");
		getChildren().addAll(timerLabel, turnTimeRemainingText);
	}

	@Override
	public void update(Observable o, Object arg) {
		String timeRemaining = String.valueOf((Integer) arg);
		turnTimeRemainingText.setText(timeRemaining);
	}

}
