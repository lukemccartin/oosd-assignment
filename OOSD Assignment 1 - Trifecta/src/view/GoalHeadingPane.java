package view;

import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;

/**
 * Holds label associated with heading in Goal set up
 * 
 *  @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */
public class GoalHeadingPane extends BorderPane{

	public GoalHeadingPane() {
		Label label = new Label("Choose Goal Positions or press Start Round to play");
		label.setPadding(new Insets(50,10,10,10));
		label.setScaleX(1.5);
		label.setScaleY(1.5);
		this.setCenter(label);
	}
}
