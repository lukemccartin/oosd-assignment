package view;

import controller.SetGoalLocationsButtonEventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import view.utilities.AlertDialog;

/**
 * Pane where players select inputs for the game such as number of players and time limit per turn.
 * 
 *  @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */
public class StartGamePane extends GridPane {

	private TextField timeLimitForEachTurnTextField;
	private ComboBox<Integer> numberOfPlayersComboBox;
	private ComboBox<String> boardSizeComboBox;
	private ComboBox<Integer> numberOfGoalsComboBox;
	private MainAppStage mainAppStage;
	
	public StartGamePane(MainAppStage mainAppStage){
		this.mainAppStage = mainAppStage;
		setPaneConfig();
		setNumberOfPlayersInputArea();
		setTimeForEachTurnInputArea();
		setBoardSizeSelectionArea();
		setGoalNumberSelectionArea();
		setStartGameButtonBox();
	}
	
	private void setStartGameButtonBox() {
		HBox startGameButtonBox = new HBox();
		Button startGameButton = new Button("Start Game");
		
		startGameButtonBox.setAlignment(Pos.BOTTOM_RIGHT);
		startGameButtonBox.getChildren().add(startGameButton);
		add(startGameButtonBox, 2, 5);
		startGameButton.setOnAction(new SetGoalLocationsButtonEventHandler(this, mainAppStage));
	}

	private void setTimeForEachTurnInputArea() {
		Label timeForEachTurnLabel = new Label("Time for Each Turn: ");
		Label secondsLabel = new Label(" seconds");
		
		timeLimitForEachTurnTextField = new TextField();
		timeLimitForEachTurnTextField.setPrefWidth (35);
		timeLimitForEachTurnTextField.setPromptText("-");
		add(timeForEachTurnLabel, 1, 2);
		
		GridPane enterSeconds = new GridPane();
		
		enterSeconds.add(timeLimitForEachTurnTextField, 1, 1);
		enterSeconds.add(secondsLabel, 2, 1);
		add(enterSeconds, 2, 2);
	}

	private void setNumberOfPlayersInputArea() {
		Label numberOfPlayersLabel = new Label("Number of Players: ");
		numberOfPlayersComboBox = new ComboBox<Integer>();
		
		numberOfPlayersComboBox.getItems().addAll(3,4,5,6);
		numberOfPlayersComboBox.setPrefWidth(90);
		numberOfPlayersComboBox.setPromptText("- Select -");
		add(numberOfPlayersLabel, 1, 1);
		add(numberOfPlayersComboBox, 2, 1);	
	}
	
	private void setBoardSizeSelectionArea() {
		Label boardSizeSelectLabel = new Label("Select Board Size");
		boardSizeComboBox = new ComboBox<String>();
		boardSizeComboBox.getItems().addAll("Small", "Medium", "Large");
		boardSizeComboBox.setPrefWidth(90);
		boardSizeComboBox.setPromptText("- Select -");
		add(boardSizeSelectLabel, 1, 3);
		add(boardSizeComboBox, 2, 3);
	}
	
	private void setGoalNumberSelectionArea() {
		Label goalAmountLabel = new Label("Select Number of Goals");
		numberOfGoalsComboBox = new ComboBox<Integer>();
		numberOfGoalsComboBox.getItems().addAll(2, 3, 4);
		numberOfGoalsComboBox.setPrefWidth(90);
		numberOfGoalsComboBox.setPromptText("- Select -");
		add(goalAmountLabel, 1, 4);
		add(numberOfGoalsComboBox, 2, 4);
	}

	public void setPaneConfig(){
		setPadding(new Insets(20, 20, 20, 20));
		setHgap(2);
		setVgap(2);
	}


	public int getNumberOfPlayersValue() {
		return numberOfPlayersComboBox.getValue();
	}
	
	public int getNumberOfGoalsValue() {
		return numberOfGoalsComboBox.getValue();
	}
	
	public int getTimeForEachTurnValue(){
		return Integer.parseInt(timeLimitForEachTurnTextField.getText());
	}
	
	public String getBoardSize() {
		return boardSizeComboBox.getValue();
	}
	
	public void setDialogBoxText(String string){
		new AlertDialog(AlertType.WARNING , string);
	}

}
