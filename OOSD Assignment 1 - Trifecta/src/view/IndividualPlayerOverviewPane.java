package view;

import controller.DragDropCardOnIndividualPlayerEventHandler;
import controller.DragOverEventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;

/**
 * Pane which contains pane which holds a reference to each individual effects pane, player number and game score
 * 
 *  @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */
public class IndividualPlayerOverviewPane extends BorderPane {

	public IndividualPlayerOverviewPane(Scene scene, int playerId) {
		Label label = new Label("Player " + (playerId));
		ScoreHBox scoreHBox = new ScoreHBox(playerId);
		PlayerEffectsPane effectsPane = new PlayerEffectsPane(scene, playerId);
		effectsPane.setOnDragOver(new DragOverEventHandler());
		effectsPane.setOnDragDropped(new DragDropCardOnIndividualPlayerEventHandler());
		this.setTop(label);
		this.setCenter(scoreHBox);
		this.setBottom(effectsPane);
	}
}
