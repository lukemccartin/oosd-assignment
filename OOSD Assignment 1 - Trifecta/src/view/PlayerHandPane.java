package view;

import java.util.List;
import java.util.Observable;
import java.util.Observer;

import controller.DragStartedDuringRoundMouseEventHandler;
import controller.RotateCardOnMouseClickedEventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.layout.FlowPane;
import model.TurnManagerImpl;
import model.interfaces.Card;
import model.interfaces.GameConstants;
import model.interfaces.Player;
import view.squareview.PlayerHandSquareView;

/**
 * Pane which contains the player's hand (cards)
 * 
 *  @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */
public class PlayerHandPane extends FlowPane implements Observer {

	private Scene scene;
	
	public PlayerHandPane(Scene scene) {
		this.scene = scene;
		setUpPane();
		createPlayerHandSquareViews();
		
		TurnManagerImpl.getSingletonInstance().addObserver(this);
	}
	
	private void setUpPane(){
		setOrientation(Orientation.HORIZONTAL);
		setPadding(new Insets(10, 0, 10, 0));
		setHgap(15); 
		setAlignment(Pos.CENTER);
	}
	
	/**
	 * for each card in the player's hand create a PlayerHandSquareView and add it to the PlayerHandPane.
	 * add a DragStartedEventHandler to each PlayerHandSquareView .
	 * PlayerHandSquareView - the individual square holding the card image
	 */
	private void createPlayerHandSquareViews(){
		for(int i=0;  i < GameConstants.PLAYER_HAND_SIZE; i++){
			PlayerHandSquareView playerHandSquareView = new PlayerHandSquareView(scene.getWidth());
			playerHandSquareView.setOnMouseClicked(new RotateCardOnMouseClickedEventHandler());
			playerHandSquareView.setOnDragDetected(new DragStartedDuringRoundMouseEventHandler());
			getChildren().add(playerHandSquareView);
		}
		
	}

	@Override
	public void update(Observable o, Object arg) {
		
		if(arg instanceof Player) {
			Player player = (Player) arg;
			List<Card> playerHand = player.getPlayerHand();
			
			
			for(int i = 0; i < GameConstants.PLAYER_HAND_SIZE; i++){
				if (i < playerHand.size()){
					//retrieve the card at a given spot in the player's hand
					Card card = playerHand.get(i);
					//set the given card as the card for the associated PlayerHandSquareView
					((PlayerHandSquareView) getChildren().get(i)).setCard(card);
					//set the image for the associated PlayerHandSquareView based on the card's type
					((PlayerHandSquareView) getChildren().get(i)).setCardImageOnSquare(ImagePatternFactory.getCardImagePattern(card.getCardType().getId()));
					//ensures all Node's previously rotated set back to original state.
					((PlayerHandSquareView) getChildren().get(i)).setRotate(0);
					
				}
				else{
					//set if the size of the player hand is smaller than the maximum then the player must have less cards and system needs to show no cards
					((PlayerHandSquareView) getChildren().get(i)).setCard(null);
					((PlayerHandSquareView) getChildren().get(i)).setCardImageOnSquare(null);
			
				}
			}
		}
	}
}
