package view;

/**
 * Enum containing the card type id's
 * 
 * @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */
public enum CardTypeEnum {

	tIntersectPathCard,
	straightPathCard,
	rightTurnPathCard,
	leftTurnPathCard,
	fourWayIntersectPathCard,
	deadEndPathCard,
	startPathCard,
	falseGoalPathCard,
	trueGoalPathCard,
	clearPathActionCard,
	goalRevealActionCard,
	landfillActionCard,
	landfillRemovalActionCard,
	logBlockActionCard,
	logBlockRemovalActionCard,
	chippedToothPersonalCard,
	hurtPawPersonalCard,
	healedToothPersonalCard,
	healedPawPersonalCard,
	skipTurnPersonalCard,
	immunityPersonalCard

}
