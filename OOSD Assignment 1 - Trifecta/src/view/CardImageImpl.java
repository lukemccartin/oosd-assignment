package view;

import javafx.scene.image.Image;
import view.interfaces.CardImage;

/**
 * Class representing a card image
 * 
 * @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */
public class CardImageImpl extends Image implements CardImage{

	private String fileString;
	
	public CardImageImpl(String fileString) {
		super(fileString);
		this.fileString = fileString;
	}

	@Override
	public String getFileString() {
		return fileString;
	}

}
