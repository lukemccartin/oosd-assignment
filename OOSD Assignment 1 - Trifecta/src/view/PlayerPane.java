package view;

import java.util.Observable;
import java.util.Observer;

import controller.UndoTurnsEventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import model.TurnManagerImpl;
import model.interfaces.Player;
import view.utilities.AlertDialog;

/**
 * Pane which contains information about the player, the PlayerHandPane and the PlayerTurnActionPane
 * 
 *  @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */
public class PlayerPane extends BorderPane implements Observer {
	
	private Text currentPlayerText = new Text();
	private Label playerPaneLabel = new Label("CURRENT PLAYER INFO: ");
	private Text currentPlayerTypeText = new Text();
	private Label undoLabel = new Label("Number of rounds to undo: ");
	private ComboBox<Integer> numberOfMovesToUndo = new ComboBox<Integer>();
	private Button undoButton;
	
	public PlayerPane(Scene scene) {
		createPlayerInfoPane();
		setCenter(new PlayerHandPane(scene));
		TurnManagerImpl.getSingletonInstance().addObserver(this);
	}
		
	private void createPlayerInfoPane(){

		GridPane playerInfoPane = new GridPane();
		playerInfoPane.setAlignment(Pos.CENTER);
		playerInfoPane.setHgap(75);
		playerInfoPane.setPadding(new Insets(8, 15, 8, 15));
		playerInfoPane.add(playerPaneLabel, 0, 0);
		playerInfoPane.add(currentPlayerText, 0, 1);
		playerInfoPane.add(currentPlayerTypeText, 1, 1);
		playerInfoPane.add(getUndoPane(), 3, 1);

		setTop(playerInfoPane);
		
	}
	private GridPane getUndoPane(){
		undoButton = new Button("Undo");
		undoButton.setOnAction(new UndoTurnsEventHandler(this));
		numberOfMovesToUndo.getItems().addAll(1, 2, 3);
		numberOfMovesToUndo.setPromptText("Select");
		GridPane undoPane = new GridPane();
		undoPane.add(undoLabel, 0, 0);
		undoPane.add(numberOfMovesToUndo, 0, 1);
		undoPane.add(undoButton, 1, 1);
		return undoPane;
	}
	
	public int getNumberOfMovesToUndoValue() {
		return numberOfMovesToUndo.getValue();
	}
	
	public void setDialogBoxText(String string){
		new AlertDialog(AlertType.WARNING , string);
	}
	
	@Override
	public void update(Observable o, Object arg) {
		if(arg instanceof Player) {
			Player player = (Player) arg;
			currentPlayerText.setText("Current Player: " + player.getPlayerId());
			currentPlayerTypeText.setText("Playing as a " + player.getPlayerType().toString());
		}
		
	}

}
