package view;

import java.util.Observable;
import java.util.Observer;
import controller.DragDropCardOnBoardForGoalPlacementEventHandler;
import controller.DragOverEventHandler;
import controller.DragStartedForGoalPlacementMouseEventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import model.BoardImpl;
import model.cardcollection.CardStack;
import model.interfaces.Board;
import model.interfaces.Card;
import model.interfaces.GameConstants;
import view.squareview.AbstractSquareView;
import view.squareview.BoardSquareView;

/**
 * Pane which contains the board.
 * 
 *  @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */
public class GoalPlacementBoardPane extends GridPane implements Observer {
	
	private AbstractSquareView[][] squareViews;
	private int numSquaresPerRow;
	private Board board;
	private Scene scene;

	public GoalPlacementBoardPane(Scene scene, int boardSize) {
		this.scene = scene;
		numSquaresPerRow = boardSize;
		squareViews = new AbstractSquareView[numSquaresPerRow][numSquaresPerRow];
		populateSquares();
		setAlignment(Pos.CENTER);
		board = BoardImpl.getSingletonInstance();
		board.addObserver(this);
		setPadding(new Insets(50, 50, 50, 50));
	}

	/**
	 * for each row and column on the board create a BoardSquareView and add it to the BoardPane.
	 * add a BoardSquareOnMouseClickedEventHandler to each BoardSquareView.
	 */
	private void populateSquares(){
		for (int row=0;  row < numSquaresPerRow; row++){
			
			for(int column=0;  column < numSquaresPerRow; column++){
				
				BoardSquareView boardSquareView = new BoardSquareView(scene.getWidth(), row, column);
				squareViews[row][column] = boardSquareView;
				
				boardSquareView.setOnDragDetected(new DragStartedForGoalPlacementMouseEventHandler());
				boardSquareView.setOnDragOver(new DragOverEventHandler());
				boardSquareView.setOnDragDropped(new DragDropCardOnBoardForGoalPlacementEventHandler());
				add(boardSquareView, column, row);
			
			}
		}
	}
		
	@Override
	public void update(Observable o, Object arg) {
		
		if(arg instanceof int[]) {
			int[] coordinates = (int[]) arg;
			int row = coordinates[0];
			int column = coordinates[1];
			
	 		
	 		CardStack square = board.getSquare(row , column);
			
	 		if (!square.getCards().isEmpty()){
		 		//get the card on top of the CardStack on the square
				Card cardLastAddedToSquare = square.getCards().peek();
				
				//if the card type is a goal path card display the card back image
		
				if(cardLastAddedToSquare.getIsCardFacingDown()){
					squareViews[row][column].setCardImageOnSquare(ImagePatternFactory.getCardBackImagePattern());
					squareViews[row][column].setRotate(0);
				}
				//otherwise display the image on the front of the card
				else {
					squareViews[row][column].setCardImageOnSquare(ImagePatternFactory.getCardImagePattern(cardLastAddedToSquare.getCardType().getId()));
					squareViews[row][column].setRotate(cardLastAddedToSquare.getCardType().getCurrentRotationAxis());
		 		}
			}
	 		else{
	 			squareViews[row][column].setFill(Color.WHITE);
	 			squareViews[row][column].setRotate(0);
	 		}
		} else if(arg instanceof String){
			String notification = (String) arg;
			if(notification.equals(GameConstants.BOARD_RESET)){
				for(int row = 0; row < numSquaresPerRow; row++){
					for(int col = 0; col < numSquaresPerRow; col++){
						squareViews[row][col].setRotate(0);
					}	
				}
			}
		}

	}
}