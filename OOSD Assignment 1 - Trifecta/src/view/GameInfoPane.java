package view;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;

/**
 * Pane which contains information about the game.
 * 
 *  @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */
public class GameInfoPane extends GridPane{
	
	public GameInfoPane(int numPlayers) {
		setHgap(75);
		setPadding(new Insets(8, 0, 8, 0));
		setUpPane(numPlayers);
		setAlignment(Pos.CENTER);
	}
	
	private void setUpPane(int numPlayers){
		Label gameInfoPanelLabel = new Label("GAME INFO: ");
		Text numPlayersText = new Text("Number of Players : " + numPlayers);

		add(gameInfoPanelLabel, 0, 0);
		add(numPlayersText, 0, 1);
		add(new RoundInfoHBox(), 1, 1);
		add(new TimerHBox(), 2, 1);
	}

}
