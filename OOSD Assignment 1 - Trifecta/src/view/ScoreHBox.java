package view;

import java.util.Observable;
import java.util.Observer;

import javafx.application.Platform;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import model.ScoreManagerImpl;
import model.interfaces.Player;
/**
 * HBox which contains the score display
 * 
 *  @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */
public class ScoreHBox extends HBox implements Observer {
	private Label scoreLabel;
	private int score;
	private int playerId;
	
	public ScoreHBox(int playerId) {
		this.playerId = playerId;
		setUpHBox();
		ScoreManagerImpl.getSingletonInstance().addObserver(this);
	}
	
	private void setUpHBox(){
		score = 0;
		scoreLabel = new Label("Score: " + score);
		getChildren().add(scoreLabel);
	}
	
	@Override
	public void update(Observable o, Object arg) {
		if(arg instanceof Player) {
			Player player = (Player) arg;
			if(player.getPlayerId() == playerId) {
				score = player.getPlayerScore();
				
				//Ensures thread safety when round ended by TimerService thread.
				Platform.runLater(new Runnable() {
					  @Override public void run() {
						  scoreLabel.setText("Score: " + score);                       
					  }
				});
				
			}
		}
		
	}

}
