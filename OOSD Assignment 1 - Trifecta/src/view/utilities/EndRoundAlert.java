package view.utilities;

import java.util.ArrayList;
import java.util.List;

import javafx.scene.control.Alert;
import model.PlayerManagerImpl;
import model.TurnManagerImpl;
import model.interfaces.Player;

/**
 * Class representing an alert dialog that displays information related to the end of a round
 * 
 * @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */
public class EndRoundAlert extends Alert{
	
	public EndRoundAlert(AlertType arg0, String string) {
		super(arg0);
		setEndOfRoundMessage(string);
		setScoreboardText();
		showAndWait();
	}

	/**
	 * sets the scoreboard text in the alert
	 */
	private void setScoreboardText() {
		List<String> scoreText = new ArrayList<String>();
		
		String scoreMessage = "SCOREBOARD: ";
		for(Player player:PlayerManagerImpl.getSingletonInstance().getAllPlayers()){
			scoreText.add("Player " + player.getPlayerId() + " (" + player.getPlayerType().getBasePlayerType() + ") has " + player.getPlayerScore() + " points");
			if(player == TurnManagerImpl.getSingletonInstance().getCurrentPlayer()){
			}
		}
		for(String string: scoreText){
			scoreMessage = scoreMessage + "\n" + string;
		}
		setContentText(scoreMessage);
	}

	/**
	 * sets the end of round message in the alert depending on the winning team and player at end of round
	 * @param winner - the playerTypeId of the winning team
	 */
	private void setEndOfRoundMessage(String winner){
		String message = "";
		
		if(winner.equals("Worker")){
			if (!(TurnManagerImpl.getSingletonInstance().getCurrentPlayer().getPlayerType().getBasePlayerType().toString().equals("Worker"))){
				message = "Oops!! \nA sabotaging beaver (Player " + TurnManagerImpl.getSingletonInstance().getCurrentPlayer().getPlayerId() + ") got confused and has won it for the workers!"; 
			}
			else{
				message = "Player " + TurnManagerImpl.getSingletonInstance().getCurrentPlayer().getPlayerId() + " found the way home!";
			}
			message = message + "\nWorker beavers win this round";
		}
		else if (winner.equals("Saboteur")){
			message = "Looks like those pesky sabotaging beavers won this round";
		}
		setHeaderText(message);
	}

}
