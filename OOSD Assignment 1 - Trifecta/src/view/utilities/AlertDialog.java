package view.utilities;

import javafx.scene.control.Alert;

/**
 * Class representing an Alert Dialog
 * 
 * @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */
public class AlertDialog extends Alert {

	public AlertDialog(AlertType alertType, String string) {
		super(alertType);
		setContentText(string);
		showAndWait();
	}

}
