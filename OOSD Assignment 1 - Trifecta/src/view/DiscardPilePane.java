package view;

import java.util.Observable;
import java.util.Observer;

import controller.DragDropCardOnDiscardPileEventHandler;
import controller.DragOverEventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.FlowPane;
import javafx.scene.paint.Color;
import model.TurnManagerImpl;
import model.cardcollection.CardStack;
import model.interfaces.Card;
import view.squareview.AbstractSquareView;
import view.squareview.DiscardPileSquareView;

/**
 * Pane which contains the discard pile sqaure view
 * 
 *  @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */
public class DiscardPilePane extends FlowPane implements Observer {
	
	private Scene scene;
	private AbstractSquareView discardSquare;
	
	public DiscardPilePane (Scene scene) {
		this.scene = scene;
		TurnManagerImpl.getSingletonInstance().addObserver(this);
		setUpPane();
	}
	
	private void setUpPane(){
		setOrientation(Orientation.VERTICAL);
		setPadding(new Insets(0, 20, 0, 10));
		setVgap(5); 
		setAlignment(Pos.CENTER);
		this.discardSquare = new DiscardPileSquareView(scene.getWidth());
		discardSquare.setOnDragOver(new DragOverEventHandler());
		discardSquare.setOnDragDropped(new DragDropCardOnDiscardPileEventHandler());
		Label label = new Label("Discard Pile");
		getChildren().add(label);
		getChildren().add(discardSquare);
	}

	@Override
	public void update(Observable o, Object arg) {
		
		if(arg instanceof CardStack) {
			CardStack discardPile = (CardStack) arg;
			if (!discardPile.getCards().isEmpty()){
				Card cardLastAddedToDiscardPile = discardPile.getCards().peek();
				discardSquare.setCardImageOnSquare(ImagePatternFactory.getCardImagePattern(cardLastAddedToDiscardPile.getCardType().getId()));
			}
			else{
				discardSquare.setFill(Color.WHITE);
			}
		}
		
	}

}