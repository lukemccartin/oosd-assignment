package view;

import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.layout.FlowPane;
import model.PlayerManagerImpl;
import model.interfaces.Player;
/**
 * Pane which contains the individual player overviews
 * 
 *  @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */
public class AllPlayersOverviewPane extends FlowPane {
	
	private Scene scene;
	
	public AllPlayersOverviewPane (Scene scene, int numberOfPlayers) {
		this.scene = scene;
		setUpPane();
		createPlayerDescriptions(numberOfPlayers);
	}
	
	private void setUpPane(){
		setOrientation(Orientation.VERTICAL);
		setPadding(new Insets(20, 10, 10, 20));
		setVgap(5); 
		setAlignment(Pos.CENTER);
	}

	private void createPlayerDescriptions(int numberOfPlayers) {
		for(Player player:PlayerManagerImpl.getSingletonInstance().getAllPlayers()) {
			
			IndividualPlayerOverviewPane playersPane = new IndividualPlayerOverviewPane(scene, player.getPlayerId());
			playersPane.setPadding(new Insets(5, 5, 5, 5));
			getChildren().add(playersPane);

		}
	}

}
