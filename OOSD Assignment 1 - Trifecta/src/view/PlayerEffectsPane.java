package view;

import java.util.HashMap;
import java.util.Observable;
import java.util.Observer;
import javafx.scene.Scene;
import javafx.scene.control.ListView;
import javafx.scene.layout.StackPane;
import model.TurnManagerImpl;
import model.interfaces.Player;
import view.squareview.PlayerStatusSquareView;
/**
 * Pane which contains personal effect descriptions applied to player
 * 
 *  @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */
public class PlayerEffectsPane extends StackPane implements Observer{
	
	private int playerId;
	private ListView<String> listView;

	public PlayerEffectsPane(Scene scene, int playerId) {
		
		this.playerId = playerId;
		TurnManagerImpl.getSingletonInstance().addObserver(this);
		PlayerStatusSquareView playerStatusSquareView = new PlayerStatusSquareView(scene.getWidth() * 1.3);
		listView = new ListView<String>();
		listView.setPrefHeight(playerStatusSquareView.getHeight());
		listView.setPrefWidth(playerStatusSquareView.getWidth());
		this.getChildren().addAll(playerStatusSquareView, listView);
	}
	
	public int getAssociatedPlayerId() {
		return playerId;
	}

	@Override
	public void update(Observable o, Object arg) {

		if(arg instanceof Player) {
			Player player = (Player) arg;
			if(player.getPlayerId() == playerId) {
				HashMap<String, String> mapOfStrings = player.getPlayerType().getListOfEffectsAppliedToPlayer();
				listView.getItems().setAll(mapOfStrings.values());
			}
		} 
	}
}
