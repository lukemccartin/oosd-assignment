package view;

import controller.StartRoundButtonEventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;

/**
 * Pane which button and associated handler for moving from
 * set up to game screen
 * 
 *  @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */
public class AcceptGoalPlacementButtonPane extends BorderPane {

	private Button okButton;
	
	public AcceptGoalPlacementButtonPane(MainAppStage mainAppStage) {
		setUpPane(mainAppStage);
	}
	
	private void setUpPane(MainAppStage mainAppStage){
		okButton = new Button("Start Round");
		okButton.setOnAction(new StartRoundButtonEventHandler(mainAppStage));
		setCenter(okButton);
		setPadding(new Insets(10, 0, 10, 0));
	}
}
