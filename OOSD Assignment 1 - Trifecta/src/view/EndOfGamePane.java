package view;

import java.util.ArrayList;
import java.util.List;

import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import model.PlayerManagerImpl;
import model.ScoreManagerImpl;
import model.TurnManagerImpl;
import model.interfaces.Player;
/**
 * Class representing the end of game pane.
 * 
 * @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */
public class EndOfGamePane extends GridPane {

	public EndOfGamePane() {
		setPaneConfig();
		addEndOfGameLabel();
		setEndOfGameMessage();
		setScoreboardText();
	}

	/**
	 * sets the end of game label
	 */
	private void addEndOfGameLabel() {
		Label label = new Label("GAME FINISHED!");
		add(label, 1, 0);
		
	}
	/**
	 * sets the scoreboard text
	 */
	private void setScoreboardText() {
		List<String> scoreText = new ArrayList<String>();
		Text scoreMessageText = new Text();
		String scoreMessage = "SCOREBOARD: ";
		for(Player player:PlayerManagerImpl.getSingletonInstance().getAllPlayers()){
			scoreText.add("Player " + player.getPlayerId() + " (" + player.getPlayerType().getBasePlayerType() + ") has " + player.getPlayerScore() + " points");
			if(player == TurnManagerImpl.getSingletonInstance().getCurrentPlayer()){
			}
		}
		for(String string: scoreText){
			scoreMessage = scoreMessage + "\n" + string;
		}
		scoreMessageText.setText(scoreMessage);
		add(scoreMessageText, 1, 4);
	}
	

	/**
	 * sets the message at the end of the game which lists the players that won the game by points
	 */
	private void setEndOfGameMessage(){
		String endOfGameMessage = "";
		Text endOfGameMessageText = new Text("");
		
		for (Player player: ScoreManagerImpl.getSingletonInstance().getWinnersByPoints()){
			endOfGameMessage = endOfGameMessage + "Player " + player.getPlayerId() + " has won the game with " + player.getPlayerScore() + " points \n";
		}
		endOfGameMessageText.setText(endOfGameMessage);
		add(endOfGameMessageText, 1, 2);
	}
	
	private void setPaneConfig(){
		setPadding(new Insets(20, 20, 20, 20));
		setHgap(2);
		setVgap(2);
	}

}
