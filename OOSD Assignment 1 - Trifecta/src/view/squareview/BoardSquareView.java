package view.squareview;

/**
 * BoardSquareView which stores its row and column in the BoardPane.
 * 
 *  @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */
public class BoardSquareView extends AbstractSquareView{
	
	private int row;
 	private int column;
 	
 	
 	public BoardSquareView(double boardWidth, int row, int column) {
 		super(boardWidth);
 		this.row = row;
 		this.column = column;
 	}
 	
 	public int getRow() {
 		return row;
 	}
 
 	public int getColumn() {
 		return column;
 	}

}
