package view.squareview;

/**
 * Class represents the visual square in which personal effects on a player are shown
 * 
 * @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */

public class PlayerStatusSquareView extends AbstractSquareView {

	public PlayerStatusSquareView(double boardWidth) {
		super(boardWidth);
	}
}
