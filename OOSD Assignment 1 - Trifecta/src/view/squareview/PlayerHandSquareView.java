package view.squareview;

import model.interfaces.Card;

/**
 * PlayerHandSquareView which stores the card for the current player based on the views position
 * in the player's hand.
 * 
 *  @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */
public class PlayerHandSquareView extends AbstractSquareView {

	private Card card;
	
	public PlayerHandSquareView(double boardWidth) {
		super(boardWidth);
		card = null;
	}

	public Card getCard() {
		return card;
	}

	public void setCard(Card card) {
		this.card = card;
	}
	
}
