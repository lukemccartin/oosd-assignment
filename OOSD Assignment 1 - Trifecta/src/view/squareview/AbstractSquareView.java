package view.squareview;

import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;

/**
 * SquareView which is either empty or displays an image of the card.
 * 
 *  @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */
public abstract class AbstractSquareView extends Rectangle{
	
	public AbstractSquareView(double boardWidth) {

		setStroke(Color.BLACK);
		setFill(Color.WHITE);
		double width = boardWidth/10;
		setWidth(width);
		setHeight(width);
	
	}
	
	public void setCardImageOnSquare(ImagePattern cardImage){
		setFill(cardImage);
	}
		
}
