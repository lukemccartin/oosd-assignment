package view.squareview;

/**
 * Class represents the visual square in which discarded cards are shown
 * 
 * @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */
public class DiscardPileSquareView extends AbstractSquareView {

	public DiscardPileSquareView(double boardWidth) {
		super(boardWidth);
	}

}
