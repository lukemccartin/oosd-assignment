package view;

import java.util.Observable;
import java.util.Observer;
import controller.CloseWindowEventHandler;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import model.BoardImpl;
import model.GameEngineImpl;
import model.RoundManagerImpl;
import model.interfaces.GameConstants;

/**
 * Class that instantiates the Gui and switches between the initial game variables selection scene and 
 * the main game scene. 
 * 
 *  @author Elizabeth Shao, Rebecca Laufer, Luke McCartin 
 *
 */
public class MainAppStage extends Application implements Observer{

	private GridPane selectNumberOfPlayersPane = new StartGamePane(this);
	private int numberOfGoals;
	private Stage primaryStage;
	private Scene startScene;
	private Scene goalScene;
	private Scene gameScene;
	private Scene endOfGameScene;
	
	@Override
	public void start(Stage primaryStage) {
		this.primaryStage = primaryStage;
		
		startScene = new Scene(selectNumberOfPlayersPane);
		this.primaryStage.setTitle("Sabotage");
		this.primaryStage.setScene(startScene);
		this.primaryStage.setOnCloseRequest(new CloseWindowEventHandler());
		RoundManagerImpl.getSingletonInstance().addObserver(this);
		
		this.primaryStage.show();
		
	}
	
	public void changeToGoalSelectScene(int numberOfPlayers, int timeLimitForEachTurn, int boardSize, int numberOfGoals){


		this.numberOfGoals = numberOfGoals;
		BorderPane borderPane = new BorderPane();
		ScrollPane scrollPane = new ScrollPane(borderPane);
		goalScene = new Scene(scrollPane, 600, 850);
		borderPane.setCenter(new GoalPlacementBoardPane(goalScene, boardSize));
		GameEngineImpl.getSingletonInstance().setUpGame(numberOfPlayers, timeLimitForEachTurn, boardSize, numberOfGoals);
		borderPane.setBottom(new AcceptGoalPlacementButtonPane(this));
		borderPane.setTop(new GoalHeadingPane());
		primaryStage.setScene(goalScene);
		setPrimaryStageSize();
		primaryStage.setMaximized(true);
	}
	
	public void changeToGameScene(int boardSize, int numberOfPlayers){
		
		BorderPane borderPane = new BorderPane();
		ScrollPane scrollPane = new ScrollPane(borderPane);
		gameScene = new Scene(scrollPane, 600, 850);
		borderPane.setTop(new GameInfoPane(numberOfPlayers));
		borderPane.setLeft(new AllPlayersOverviewPane(gameScene, numberOfPlayers));
		borderPane.setRight(new DiscardPilePane(gameScene));
		borderPane.setCenter(new BoardPane(gameScene, boardSize));
		borderPane.setBottom(new PlayerPane(gameScene));
		primaryStage.setScene(gameScene);
		setPrimaryStageSize();
		primaryStage.setMaximized(true);

		RoundManagerImpl.getSingletonInstance().startRound();
		
	}
	
	private void changeToGoalSelectSceneBetweenRounds() {
		BoardImpl.getSingletonInstance().setUpBoard(numberOfGoals);
		primaryStage.setScene(goalScene);
		setPrimaryStageSize();
		primaryStage.setMaximized(true);
	}
	
	public void revertToGameScene(){
		RoundManagerImpl.getSingletonInstance().startRound();
		primaryStage.setScene(gameScene);
		setPrimaryStageSize();
		primaryStage.setMaximized(true);

	}

	public void changeToEndOfGameScene(){
		Pane endOfGamePane = new EndOfGamePane();
		endOfGameScene = new Scene(endOfGamePane);
		primaryStage.setScene(endOfGameScene);
		primaryStage.sizeToScene();
	}
	
	
	public static void main(String[] args){
		Application.launch(args);
		
	}

	@Override
	public void update(Observable o, Object arg) {
		
		if(arg instanceof String){
			String transition = (String) arg;
			if(transition.equals(GameConstants.TRANSITION_ROUNDS)){
				changeToGoalSelectSceneBetweenRounds();
			}
			if(transition.equals(GameConstants.END_OF_GAME)){
				changeToEndOfGameScene();
			}
			
		}
		
	}
	
	private void setPrimaryStageSize(){
		primaryStage.setMinHeight(750);
		primaryStage.setMinWidth(950);
		primaryStage.setHeight(900);
		primaryStage.setWidth(950);
	}

}
