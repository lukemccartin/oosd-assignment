package view;

import java.util.Observable;
import java.util.Observer;

import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import model.RoundManagerImpl;
import model.interfaces.GameConstants;

/**
 * HBox which contains the round information
 * 
 *  @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */
public class RoundInfoHBox extends HBox implements Observer {

	private Text currentRoundText;
	
	public RoundInfoHBox() {
		setUpHBox();
		RoundManagerImpl.getSingletonInstance().addObserver(this);
	}
	
	private void setUpHBox(){
		Label roundInfoLabel = new Label("Current Round : ");
		currentRoundText = new Text();
		Text numTotalRoundsText = new Text("/" + GameConstants.NUM_ROUNDS);
		getChildren().addAll(roundInfoLabel, currentRoundText, numTotalRoundsText);
	}
	
	@Override
	public void update(Observable o, Object arg) {
		if(arg instanceof Integer) {
			Integer currentRound = (Integer) arg;
			currentRoundText.setText(currentRound.toString());
		}
		
	}

}
