package model;

import java.util.ArrayList;

import java.util.List;
import java.util.Observable;
import model.interfaces.Player;
import model.interfaces.ScoreManager;

/**
 * Class representing the score manager which handles the calculation of player scores after each round
 * by evaluating the winning player type at the end of each round and distributing points to the appropriate
 * players of that player type.
 * 
 * @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */

public class ScoreManagerImpl extends Observable implements ScoreManager {
	private static ScoreManager singletonInstance;
	private int prizePool;
	private List<Player> playersWithHighestScore = new ArrayList<Player>();
	
	private ScoreManagerImpl(){
		prizePool = PlayerManagerImpl.getSingletonInstance().getAllPlayers().size() * 10;
	}
	
	public static ScoreManager getSingletonInstance(){
		if (singletonInstance == null){
			singletonInstance = new ScoreManagerImpl();
		}
		return singletonInstance;
	}
	
	/**
	 * sets the player(s) with the highest points
	 */
	private void setWinnersByPoints(){
		List<Player> playerList = new ArrayList<Player>();
		
		playerList.addAll(PlayerManagerImpl.getSingletonInstance().getAllPlayers());
	
		for(Player player: playerList){
			if(playersWithHighestScore.isEmpty()){
				playersWithHighestScore.add(player);
			}
			else{
				for(Player highScorePlayer: playersWithHighestScore){
					if(player.getPlayerScore() > highScorePlayer.getPlayerScore()){
						playersWithHighestScore.add(player);
						playersWithHighestScore.remove(highScorePlayer);
					}
					else if(player.getPlayerScore() == highScorePlayer.getPlayerScore()){
						playersWithHighestScore.add(player);
					}
				}
			}
		}
	}
	
	@Override
	public List<Player> getWinnersByPoints(){
		setWinnersByPoints();
		return playersWithHighestScore;
	}

	@Override
	public void calculateScores(String winningPlayerType, Player currentPlayerAtEndOfRound) {
		ArrayList<Player> winningPlayers = new ArrayList<Player>();
		int winnersShare = 0;
		int dividend = 0;

		for(Player player: PlayerManagerImpl.getSingletonInstance().getAllPlayers()){
			if (player.getPlayerType().getBasePlayerType().toString().equals(winningPlayerType)){
				winningPlayers.add(player);				
			}
		}
		
		//if the workers win and the player that ended the round is a worker then that player gets a greater portion of the prize pool
		if(winningPlayerType.equals("Worker") && currentPlayerAtEndOfRound.getPlayerType().getBasePlayerType().toString().equals(winningPlayerType)){
			winnersShare = prizePool/2;
			prizePool = prizePool-winnersShare;
			currentPlayerAtEndOfRound.setPlayerScore(winnersShare);
		}
		
		//distribute points to winners
		dividend = prizePool/winningPlayers.size();
		for(Player player: winningPlayers){
			player.setPlayerScore(dividend);
			notifyObserversOfScoreChange(player);
		}
		
	}
	
	private void notifyObserversOfScoreChange(Player player){
		setChanged();
		notifyObservers(player);
	}


}
