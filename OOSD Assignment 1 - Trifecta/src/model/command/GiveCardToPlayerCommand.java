package model.command;

/**
 * Class representing a command for giving a card to another player.
 * 
 * @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */
public class GiveCardToPlayerCommand extends AbstractCommand {

	private int toPlayerId;
	
	public GiveCardToPlayerCommand(int toPlayerId) {
		this.toPlayerId = toPlayerId;
	}

	@Override
	public boolean execute() {
		return turnManager.giveCardToPlayer(toPlayerId);
	}

	@Override
	public void undo() {
		turnManager.undoGiveCardToPlayer(player, toPlayerId);
	}

}
