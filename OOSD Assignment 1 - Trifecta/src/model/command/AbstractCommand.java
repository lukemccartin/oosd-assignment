package model.command;

import model.TurnManagerImpl;
import model.interfaces.Command;
import model.interfaces.Player;
import model.interfaces.TurnManager;

/**
 * Class representing an AbstractCommand which holds a reference to the
 * receiver (TurnManager) and the current player for that turn which 
 * is shared by all commands
 * 
 * @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */
public abstract class AbstractCommand implements Command {

	protected TurnManager turnManager;
	protected Player player;
	
	public AbstractCommand() {
		turnManager = TurnManagerImpl.getSingletonInstance();
		player = turnManager.getCurrentPlayer();
	}
			
}
