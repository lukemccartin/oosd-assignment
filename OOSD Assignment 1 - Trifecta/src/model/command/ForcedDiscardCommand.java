package model.command;

/**
 * Class representing a command for the forced discard of a card.
 * 
 * @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */
public class ForcedDiscardCommand extends AbstractCommand {
	
	@Override
	public boolean execute() {
		return turnManager.forcedDiscard();
	}

	@Override
	public void undo() {
		turnManager.undoDiscardCard(player);
	}

}
