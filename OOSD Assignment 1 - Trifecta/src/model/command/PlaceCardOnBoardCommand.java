package model.command;

/**
 * Class representing a command for placing a card on the board.
 * 
 * @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */
public class PlaceCardOnBoardCommand extends AbstractCommand {

	private int row;
	private int column;
	
	public PlaceCardOnBoardCommand(int row, int column) {
		this.row = row;
		this.column = column;
	}

	@Override
	public boolean execute() {
		return turnManager.placeCardOnBoard(row, column);
	}

	@Override
	public void undo() {
		turnManager.undoPlaceCardOnBoard(row, column, player);
	}

}
