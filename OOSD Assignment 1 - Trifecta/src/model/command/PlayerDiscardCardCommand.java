package model.command;

/**
 * Class representing a command for the voluntary discard of a card by a player.
 * 
 * @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */
public class PlayerDiscardCardCommand extends AbstractCommand {
	
	@Override
	public boolean execute() {
		return turnManager.playerDiscardCard();
	}

	@Override
	public void undo() {
		turnManager.undoDiscardCard(player);
	}

}
