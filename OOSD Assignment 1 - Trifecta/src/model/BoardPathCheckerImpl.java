package model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Stack;
import model.cardcollection.CardStack;
import model.interfaces.BoardPathChecker;
import model.interfaces.PathNode;

/**
 * Class representing the board path checker which checks if paths from the start 
 * to the goal are clear of obstacles and complete
 * 
 * @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */

public class BoardPathCheckerImpl implements BoardPathChecker{
	private static BoardPathChecker singletonInstance;
	
	private BoardPathCheckerImpl() {

	}
	
 	public static BoardPathChecker getSingletonInstance(){
 	    if(singletonInstance == null)
            singletonInstance = new BoardPathCheckerImpl();

        return singletonInstance;
 	}
	

 	@Override
	public List<PathNode> getNeighbouringSuccessors(PathNode pathNode){
		
		List<PathNode> neighbouringSuccessors = new ArrayList<PathNode>();

		neighbouringSuccessors = BoardSquareNeighbourCheckerImpl.getSingletonInstance().getAccessibleNeighbours(pathNode);
		
		return neighbouringSuccessors;
	}

 	
 	@Override
	public boolean depthFirstSearch(int[] startCoordinate, int[] goalCoordinate){
		//a set which holds the nodesVisted as the path is traversed
 		Set<int[]> nodesVisited = new HashSet<int[]>();
		//stack of nodes to be visited
 		Stack<PathNode> fringe = new Stack<PathNode>();
		
	   
	    //create a start path node which contains the coordinate for the start square and the start square's cardstack
		int row = startCoordinate[0];
		int column = startCoordinate[1];
		CardStack cardStack = BoardImpl.getSingletonInstance().getSquare(row, column);
		PathNode startNode = new PathNodeImpl(cardStack, row, column);
		
		//push the start node onto the fringe
		fringe.push(startNode);
		
		while(!fringe.isEmpty()){
			PathNode currentNode = fringe.pop();
			
			//if the board coordinate of the current path node is the goal return true
			if(currentNode.getCoordinate()[0] == goalCoordinate[0] && currentNode.getCoordinate()[1] == goalCoordinate[1]){
	            return true;
	        }
			
			boolean nodeHasBeenVisited = false;
	        
			//check if the board coordinate of the current path node has been visited before
	        for(int[] coordinate: nodesVisited){
	        	if(currentNode.getCoordinate()[0] == coordinate[0] && currentNode.getCoordinate()[1] == coordinate[1])
	        		nodeHasBeenVisited = true;
	        }
	        
	        /*
	         * if the coordinate of the current path node has not been visited yet, add the coordinate to the
	         * nodes visited set and retrieve the path nodes of its neighbours on the board
	         */
	        if(!nodeHasBeenVisited){
	            nodesVisited.add(currentNode.getCoordinate());
	         
	            List<PathNode> successors = getNeighbouringSuccessors(currentNode);
	            
	            /*
	             * for each successor, push the successor path node onto the fringe
	             */
	            for(PathNode successor: successors){
	            	fringe.push(successor);
	            }
	        }
		}
		return false;
	}
	
}
