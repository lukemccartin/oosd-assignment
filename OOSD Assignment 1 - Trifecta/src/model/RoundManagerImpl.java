package model;

import java.util.Observable;

import com.google.java.contract.Ensures;

import javafx.application.Platform;

import model.cardcollection.CardStack;
import model.cardcollection.DeckImpl;
import model.interfaces.Board;
import model.interfaces.Card;
import model.interfaces.CardType;
import model.interfaces.GameConstants;
import model.interfaces.PlayerManager;
import model.interfaces.RoundManager;
import model.interfaces.TurnManager;

/**
 * Class representing the round manager which starts a new round, ends the round
 * and stores the number of the current round.
 * 
 * @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */
public class RoundManagerImpl extends Observable implements RoundManager {

	private static RoundManager singletonInstance;
	private CardStack discardPile;
	private TurnManager turnManager;
	private PlayerManager playerManager;
	private DeckImpl deck;
	private Board board;
	private int currentRound;
	
	private RoundManagerImpl() {
		currentRound = 0;
	}
	
	public static RoundManager getSingletonInstance(){
 	    if(singletonInstance == null)
            singletonInstance = new RoundManagerImpl();
 	    
        return singletonInstance;
 	}
	
 	@Override
	@Ensures("currentRound == old(currentRound + 1)")
	public void startRound(){
 		currentRound++;
 		setChanged();
 		notifyObservers(currentRound);
 		
		turnManager = TurnManagerImpl.getSingletonInstance();
		playerManager = PlayerManagerImpl.getSingletonInstance();
		board = BoardImpl.getSingletonInstance();
 		deck = DeckImpl.getSingletonInstance();
		discardPile = GameEngineImpl.getSingletonInstance().getDiscardPile();
 		
 		playerManager.assignPlayerTypeToPlayers();
 		deck.shuffleDeck();
 		board.updateGameBoardWithStartAndGoalLocations();
		DealerImpl.getSingletonInstance().dealCardsToPlayersAtStartOfRound();
		turnManager.startFirstTurn();
		CommandManagerImpl.getSingletonInstance().clearCommands();
		playerManager.resetUndosUsedPerPlayer();
	}
 	
 	@Override
 	public void endRound(){

 		BoardImpl.getSingletonInstance().clearBoard();
 		/*
 		 * whilst there are still cards in the discard pile,
 		 * removing each card from the discard pile, reset its rotation
 		 * and put it back in the deck.
 		 */
 		while(!discardPile.getCards().isEmpty()){
 			Card card = discardPile.removeCard();
 			CardType cardType = card.getCardType();
 			
 			while(cardType.getCurrentRotationAxis() != 0){
 				cardType.rotateCard90Degrees();
 			}
 			
 			deck.addCard(card);
 		}

 		if(currentRound < 3) {

 			Platform.runLater(new Runnable() {
 				public void run() {
 					setChanged();
 					notifyObservers(GameConstants.TRANSITION_ROUNDS);    
 				}
 			});

 		} else {

			Platform.runLater(new Runnable() {
 				public void run() {
 					setChanged();
 					notifyObservers(GameConstants.END_OF_GAME);    
 				}
 			});
 		
 		}

 	}
 	
	@Override
	public int getCurrentRound() {
		return currentRound;
	}
	
}
