package model;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Random;

import javafx.scene.control.Alert.AlertType;
import model.card.CardImpl;
import model.card.cardfactory.PathCardFactory;
import model.cardcollection.CardStack;
import model.cardcollection.DeckImpl;
import model.exception.CardMovementException;
import model.exception.PlaceabilityException;
import model.interfaces.Board;
import model.interfaces.Card;
import model.interfaces.CardType;
import model.interfaces.GameConstants;
import view.utilities.AlertDialog;

/**
 * Class representing the board.
 * 
 * @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */
public class BoardImpl extends Observable implements Board {

	private static Board singletonInstance;
	private CardStack[][] squares;
	private int numSquaresPerRow;
	private List<int[]> startAndGoalCoordinates;
	private int[] startCoordinate = new int[2];
	private int[] trueGoalCoordinate = new int[2];
	private int[] coordinatesForGoalMovementByPlayer = new int[2];
	
	private BoardImpl() {
		
	}
	
 	public static Board getSingletonInstance(){
 	    if(singletonInstance == null)
            singletonInstance = new BoardImpl();

        return singletonInstance;
 	}
	
	@Override
	public void setUpBoard(int numberOfGoals){
		setStartSquare();
		setGoalSquares(numberOfGoals);
	}
	
	@Override
	public int getBoardSize(){
		return numSquaresPerRow;
	}

	@Override
	public void createSquaresOnBoard(int boardSize){
		numSquaresPerRow = boardSize;
		squares = new CardStack[numSquaresPerRow][numSquaresPerRow];
		startAndGoalCoordinates = new ArrayList<int[]>();
		for (int row=0; row < numSquaresPerRow; row++){
			for(int column=0; column < numSquaresPerRow; column++){
					squares[row][column] = new CardStack();
			}

		}
		
	}
	
	//Handles board clearing between rounds
	@Override
	public void clearBoard(){
		clearStartAndGoalCards();
		
		for (int row=0; row < numSquaresPerRow; row++){
			for(int column=0; column < numSquaresPerRow; column++){
				while (!squares[row][column].getCards().isEmpty()){
					Card card = squares[row][column].removeCard();	
					CardType cardType = card.getCardType();
					
		 			while(cardType.getCurrentRotationAxis() != 0){
		 				cardType.rotateCard90Degrees();
		 			}
					
		 			DeckImpl.getSingletonInstance().addCard(card);
				}
				notifyObserversOfSquareCardStackChange(row, column);
			}
		}
	}
	
	
	@Override
	public CardStack getSquare(int row, int column){
		return squares[row][column];
	}
	
	
	@Override
	public boolean addCardToSquare(int row, int column, Card card){
		boolean cardAdded = false;
		
		CardStack square = getSquare(row,column);
		
		/*
		 * check if card can be placed on square, if it can then add it to the square.
		 */
		try{
			if (card.getCardType().getCardPlaceability().placeCardOnSquare(square, card, row, column)){
				square.addCard(card);
				
				notifyObserversOfSquareCardStackChange(row, column);
				cardAdded = true;
			}
		} catch(PlaceabilityException pe){
			new AlertDialog(AlertType.ERROR , "Cannot place card on that square");
		}
		
		return cardAdded;
	}
	
	
	@Override
	public void removeCardFromSquare(int row, int column, Card card){
		squares[row][column].removeCard();
		notifyObserversOfSquareCardStackChange(row, column);
	}
	
		
	/**
	 * for the last row on the board randomly select a column and set it as the start square.
	 * add the start path card to the square chosen.
	 */
	private void setStartSquare(){
		int row = numSquaresPerRow-1;
		int column = new Random().nextInt(numSquaresPerRow);
		startCoordinate[0] = row;
		startCoordinate[1] = column;
		
		CardImpl startCard = new CardImpl(PathCardFactory.getSingletonInstance().createCardType("startPathCard"));
		
		startCard.setIsCardFacingDown(false);
		squares[row][column].addCard(startCard);
		storeStartAndGoalCoordinates(row, column);
		notifyObserversOfSquareCardStackChange(row, column);
	}
	
	/**
	 * randomly allocated 3 or 4 goals to squares in the first two rows on the board.
	 * ensure that goals are not allocated in adjacent squares.
	 */
	private void setGoalSquares(int numberOfGoals) {

		Random random = new Random();
		int i = 0;
		int row = random.nextInt(2);
		int col = new Random().nextInt(numSquaresPerRow);
		trueGoalCoordinate[0] = row;
		trueGoalCoordinate[1] = col;
		
		squares[row][col].addCard(new CardImpl(PathCardFactory.getSingletonInstance().createCardType("trueGoalPathCard")));
		storeStartAndGoalCoordinates(row, col);
		notifyObserversOfSquareCardStackChange(row, col);
		
		while(i < numberOfGoals - 1) {
			row = random.nextInt(2);
			col = new Random().nextInt(numSquaresPerRow);
			if(squares[row][col].getCards().isEmpty()) {
				/*
				 * if the column to be allocated is the first column check the square in the same 
				 * row but one column to the right is empty before selecting as goal square
				 */
				if (col == 0) {
					if(squares[row][col + 1].getCards().isEmpty()) {
						squares[row][col].addCard(new CardImpl(PathCardFactory.getSingletonInstance().createCardType("falseGoalPathCard")));
						storeStartAndGoalCoordinates(row, col);
						notifyObserversOfSquareCardStackChange(row, col);
						i++;
					}
				/*
				 * if the column to be allocated is the last column check the square in 
				 * the same row but one column to the left is empty before selecting as goal square
				 */
				} else if (col == numSquaresPerRow - 1) {
					if (squares[row][col - 1].getCards().isEmpty()) {
						squares[row][col].addCard(new CardImpl(PathCardFactory.getSingletonInstance().createCardType("falseGoalPathCard")));
						storeStartAndGoalCoordinates(row, col);
						notifyObserversOfSquareCardStackChange(row, col);
						i++;
					}
				/*
				 * check the squares in the same row but one column to the left and right 
				 * are empty before selecting as goal square.
				 */
				} else {
					if(squares[row][col - 1].getCards().isEmpty() && 
							squares[row][col + 1].getCards().isEmpty()) {

						squares[row][col].addCard(new CardImpl(PathCardFactory.getSingletonInstance().createCardType("falseGoalPathCard")));
						storeStartAndGoalCoordinates(row, col);
						notifyObserversOfSquareCardStackChange(row, col);
						i++;
					}
				}
			}
		}

	}
	
	@Override
	public void setGoalCardMovementCoordinates(int row, int col) {
		coordinatesForGoalMovementByPlayer[0] = row;
		coordinatesForGoalMovementByPlayer[1] = col;
	}
	
	@Override
	public void handleGoalCardMovement(int row, int col) throws CardMovementException {
		
		if(row > 1) {
			throw new CardMovementException("Goal must be placed in top two rows.");
		}
		
		//Checks for appropriate placement of goal by player
		for (int[] coordinates : startAndGoalCoordinates) {
			
			if(coordinates[0] == row && 
					coordinates[1] == col) {
				throw new CardMovementException("Cannot place on already occupied square.");
			}
			
			/*
			 * Checks to ensure card being moved cannot be placed in a square directly
			 * adjacent to an already existing goal on the board, taking into consideration
			 * that it can be placed next to itself if it does not violate the afformentioned
			 * rule.
			 */
			if (!(coordinates[0] == coordinatesForGoalMovementByPlayer[0] && 
					coordinates[1] == coordinatesForGoalMovementByPlayer[1])){
				
				if((row == (coordinates[0] + 1) && col == (coordinates[1])) 
					|| (row == (coordinates[0] - 1) && col == (coordinates[1]))
					|| (col == (coordinates[1] + 1) && row == (coordinates[0]))
					|| (col == (coordinates[1] - 1) && row == (coordinates[0]))){
					throw new CardMovementException("Goal must not be directly next to other goal.");
				}
			}	
		}
		
		//Handles movement of goal into the newly dropped on square.
		for(int[] coordinates : startAndGoalCoordinates) {
			if(coordinates[0] == coordinatesForGoalMovementByPlayer[0] && 
					coordinates[1] == coordinatesForGoalMovementByPlayer[1]) {
				coordinates[0] = row;
				coordinates[1] = col;
			}
		}
		
		//Updates coordinates of the true goal if the goal moved was the true goal also
		if(coordinatesForGoalMovementByPlayer[0] == trueGoalCoordinate[0] && 
				coordinatesForGoalMovementByPlayer[1] == trueGoalCoordinate[1]){
			trueGoalCoordinate[0] = row;
			trueGoalCoordinate[1] = col;
		}
		
		CardStack oldCardPosition = getSquare(coordinatesForGoalMovementByPlayer[0],coordinatesForGoalMovementByPlayer[1]);
		Card cardToMove = oldCardPosition.removeCard();
		notifyObserversOfSquareCardStackChange(coordinatesForGoalMovementByPlayer[0],coordinatesForGoalMovementByPlayer[1]);
		CardStack newLocation = getSquare(row, col);
		newLocation.addCard(cardToMove);
		
		notifyObserversOfSquareCardStackChange(row, col);
		
	}
	
	@Override
	public void updateGameBoardWithStartAndGoalLocations(){
		for (int[] coordinates : startAndGoalCoordinates) {
			setChanged();
			notifyObservers(coordinates);
		}
	}
	
	private void notifyObserversOfSquareCardStackChange(int row, int column){
		int[] coordinates = new int [2];
		coordinates[0] = row;
		coordinates[1] = column; 
		
		setChanged();
		notifyObservers(coordinates);
	}

	@Override
	public void flipCard(int row, int column) {
		getSquare(row, column).getCards().peek().setIsCardFacingDown(false);
		notifyObserversOfSquareCardStackChange(row, column);
	}
	
	@Override
	public void checkNeighboursToFlip(int row, int column) {
		//check all neighbouring squares on board to see if there are any cards to flip
		if(row > 0 && !getSquare(row-1, column).getCards().isEmpty()){
				flipCard(row-1, column);
		}
		if(row < numSquaresPerRow-1 && !getSquare(row+1, column).getCards().isEmpty()){
			flipCard(row+1, column);
		}
		if(column > 0 && !getSquare(row, column-1).getCards().isEmpty()){
			flipCard(row, column-1);
		}
		if(column < numSquaresPerRow-1 && !getSquare(row, column+1).getCards().isEmpty()){
			flipCard(row, column+1);
		}
	}

	private void storeStartAndGoalCoordinates(int row, int column){
		int[] coordinates = new int [2];
		coordinates[0] = row;
		coordinates[1] = column; 
		startAndGoalCoordinates.add(coordinates);
	}
	
	private void clearStartAndGoalCards(){
		for(int[] coordinate: startAndGoalCoordinates){
			squares[coordinate[0]][coordinate[1]].removeCard();	
		}
		startAndGoalCoordinates.clear();
		setChanged();
		notifyObservers(GameConstants.BOARD_RESET);
	}
	@Override
	public int[] getStartCoordinate(){
		return startCoordinate;
	}
	@Override
	public int[] getTrueGoalCoordinate(){
		return trueGoalCoordinate;
	}


}
