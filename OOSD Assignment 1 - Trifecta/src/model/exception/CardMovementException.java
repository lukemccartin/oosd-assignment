package model.exception;

/**
 * Class that represents exception when an attempt is made at moving a card 
 * but it is not allowable
 * 
 * @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */
public class CardMovementException extends Exception {

	public CardMovementException() {
		super();
	}

	public CardMovementException(String message) {
		super(message);
	}

}