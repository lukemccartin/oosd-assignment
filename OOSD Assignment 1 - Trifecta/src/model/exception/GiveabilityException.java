package model.exception;

/**
 * Class that represents exception when an attempt is made at passing cards 
 * between players but it is not allowable
 * 
 * @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */
public class GiveabilityException extends Exception {

	public GiveabilityException() {
		super();
	}

	public GiveabilityException(String message) {
		super(message);
	}

}
