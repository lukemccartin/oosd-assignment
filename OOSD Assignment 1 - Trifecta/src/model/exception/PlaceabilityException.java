package model.exception;

/**
 * Class that represents exception when an attempt is made at placing card
 * but it is not allowable
 * 
 * @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */
public class PlaceabilityException extends Exception {

	public PlaceabilityException() {
		super();
	}

	public PlaceabilityException(String message) {
		super(message);
	}

}
