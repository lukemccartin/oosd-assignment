package model;

import java.util.Observable;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import model.command.ForcedDiscardCommand;

/**
 * Class representing the timer for each turn.
 * 
 * @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */
public class TurnTimerService extends Observable {

	private int countdown;	
	private final ScheduledExecutorService timer = 
			Executors.newScheduledThreadPool(1);
	
	private final ScheduledExecutorService timerDisplay = 
			Executors.newScheduledThreadPool(1);
	
	ScheduledFuture<?> forceTurnEndHandle;
	ScheduledFuture<?> forceDisplayTimerHandle;
	
	/**
	 * Updates the view to display the time left in the current player's turn
	 * 
	 * @param time - time limit per turn
	 */
	public void setCountdownTimerForDisplay(int time) {
		countdown = time;
		final Runnable visibleTimer = new Runnable() {
			public void run() {
				countdown--;
				setChanged();
				notifyObservers(countdown);
				}
		};
		// Scheduled to call the runnable method visibleTimer every second
		forceDisplayTimerHandle = timerDisplay.scheduleAtFixedRate(visibleTimer, 1, 1, TimeUnit.SECONDS);
	}
	
	/**
	 * sets a timer given the specified time limit per turn
	 * 
	 * @param turnTime - time limit per turn
	 */
	public void setSelectedTurnTimer(int turnTime) {
		final Runnable forceTurnEnd = new Runnable() { 
			public void run() { 
				CommandManagerImpl.getSingletonInstance().storeAndExecute(new ForcedDiscardCommand());
			}
		};
		
		//Forces the end of a players turn once timer ends by calling the runnable method forceEndTurn
		forceTurnEndHandle = timer.schedule(forceTurnEnd, turnTime, TimeUnit.SECONDS);
		
	}
	
	public void cancelCurrentTurnTimer() {
		forceTurnEndHandle.cancel(true);
	}
	
	public void resetCountdownTimer() {
		forceDisplayTimerHandle.cancel(true);
	}
}
