package model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Observable;

import com.google.java.contract.Ensures;
import com.google.java.contract.Requires;

import model.interfaces.Player;
import model.interfaces.PlayerManager;
import model.player.PlayerImpl;
import model.player.Saboteur;
import model.player.Worker;


/**
 * Class representing the player manager which creates the players, allocates their type
 * and stores the players
 * 
 * @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */
public class PlayerManagerImpl extends Observable implements PlayerManager {

	private static PlayerManager singletonInstance;
	private List<Player> players;
	private int numWorkers;
	private int numSaboteurs;
	private Map<Player, Integer> undosUsedPerPlayer;
	
	private PlayerManagerImpl() {
		players = new ArrayList<Player>();
		undosUsedPerPlayer = new HashMap<Player, Integer>();		
	}
	
 	public static PlayerManager getSingletonInstance(){
 	    if(singletonInstance == null)
            singletonInstance = new PlayerManagerImpl();

        return singletonInstance;
 	}
 	
 	@Requires("numPlayers>0")
 	@Ensures("players.size() == numPlayers")
 	@Override
	public void createPlayers(int numPlayers){
 		
 		setNumberOfWorkersAndSaboteurs(numPlayers);
 			
		for (int i = 1; i <= numPlayers; i ++){
			Player player = new PlayerImpl(i);
			addPlayer(player);
		}
		
		assignPlayerTypeToPlayers();
		
		for(Player player: players){
			undosUsedPerPlayer.put(player, 0);
		}
			
	}
		
 	/**
 	 * set the ratio of workers to saboteurs given the number of players
 	 * @param numPlayers - the number of players in the game
 	 */
 	private void setNumberOfWorkersAndSaboteurs(int numPlayers){
		numSaboteurs = numPlayers/2;
		
		if (numPlayers % 2 == 1){
			numWorkers = numPlayers/2 + 1;
		}
		else {
			numWorkers = numPlayers/2;
		}
 	}
 	
 	/**
 	 * for each player in the players list assign them a player type randomly whilst
 	 * adhering to the ratio of workers to saboteurs in the game
 	 */
 	public void assignPlayerTypeToPlayers(){
		
 		int workersLeftToAllocate = numWorkers;
		int saboteursLeftToAllocate = numSaboteurs;
 		
 		for(Player player: players){	
 			
 			//if there are no workers left to allocate assign the player type as saboteur
 			if(workersLeftToAllocate == 0){
 				player.setPlayerType(new Saboteur());
 				saboteursLeftToAllocate--;
 			}
 			//if there are no saboteurs left to allocate assign the player type as worker 
 			else if(saboteursLeftToAllocate == 0){
 			 	player.setPlayerType(new Worker());
 				workersLeftToAllocate--;
 			}
 			//otherwise randomly allocate player type
 			else{
 				if (Math.random() > 0.5) {
 					player.setPlayerType(new Worker());
 					workersLeftToAllocate--;
 				}
 				else {
 					player.setPlayerType(new Saboteur());
 					saboteursLeftToAllocate--;
 				}	
 			
 			}
 		}	
	}

	private void addPlayer(Player player) {
		players.add(player);
	}
			
	@Override
	public Player getPlayer(int id) {
		return players.get(id);
	}

	@Override
	public List<Player> getAllPlayers() {
		return players;
	}
		
	@Override
	public void resetUndosUsedPerPlayer(){
		for(Player player: undosUsedPerPlayer.keySet()){
			undosUsedPerPlayer.replace(player,0);
		}
	}

	@Override
	public void incrementUndosUsedByPlayer(Player player){
		Integer numUndos = undosUsedPerPlayer.get(player);
		numUndos++;
		undosUsedPerPlayer.replace(player,numUndos);
	}
	
	@Override
	public Integer getUndosUsedByPlayer(Player player){
		return undosUsedPerPlayer.get(player);
	}
	
	@Override
	public boolean checkPlayerHandsAreEmpty(){
		boolean playerHandCheck = false;
		int emptyCount = 0;
		
		for (Player player:players){
			if(player.getPlayerHand().isEmpty()){
				emptyCount++;
			}
		}
		
		if(emptyCount == players.size()){
			playerHandCheck = true;
		}

		return playerHandCheck;
	}
}
