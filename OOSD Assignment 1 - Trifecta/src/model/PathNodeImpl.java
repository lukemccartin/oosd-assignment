package model;

import model.cardcollection.CardStack;
import model.interfaces.PathNode;

/**
 * Class representing a node in a path
 * 
 * @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */
public class PathNodeImpl implements PathNode{
	
	private CardStack square;
	private int[] coordinate = new int[2];
	
	public PathNodeImpl(CardStack square, int row, int column){
		this.square = square;
		setCoordinate(row, column);
	}
	
	@Override
	public int[] getCoordinate(){
		return coordinate;
	}
	
	@Override
	public CardStack getSquare(){
		return square;
	}
	
	/**
	 * given a set of coordinates, set the coordinates of this node
	 * @param row - a row coordinate on the board
	 * @param column - a column coodrinate on the board
	 */
	private void setCoordinate(int row, int column){
		this.coordinate[0] = row;
		this.coordinate[1] = column;
	}	
}
