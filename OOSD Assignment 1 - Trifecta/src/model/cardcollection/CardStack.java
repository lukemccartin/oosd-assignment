package model.cardcollection;

import java.util.Stack;

import com.google.java.contract.Ensures;
import com.google.java.contract.Requires;

import model.interfaces.Card;
import model.interfaces.CardCollection;

/**
 * Class representing a stack of cards.
 * 
 * @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */
public class CardStack implements CardCollection {

	private Stack<Card> cards;
	
	public CardStack() {
		cards = new Stack<Card>();
	}

	@Override
	@Requires("card != null")
	@Ensures("cards.size() == old(cards.size() + 1)")
	public boolean addCard(Card card) {
		if(cards.push(card) != null)
			return true;
		return false;
	}
	
	@Requires("!cards.isEmpty()")
	@Ensures("cards.size() == old(cards.size() - 1)")
	public Card removeCard() {
		return cards.pop();
	}
	
	@Override
	public Stack<Card> getCards() {
		return cards;
	}
	
}
