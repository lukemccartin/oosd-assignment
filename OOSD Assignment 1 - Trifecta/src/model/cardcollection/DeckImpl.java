package model.cardcollection;

import java.util.Collections;

import model.BoardImpl;
import model.card.CardImpl;
import model.card.cardfactory.ActionCardFactory;
import model.card.cardfactory.PathCardFactory;
import model.card.cardfactory.PersonalCardFactory;
import model.interfaces.Deck;
import model.interfaces.GameConstants;
import model.interfaces.GameConstants.ActionCardType;
import model.interfaces.GameConstants.PlayablePathCardType;
import model.interfaces.GameConstants.PersonalCardType;

/**
 * Class representing the deck.
 * 
 * @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */
public class DeckImpl extends CardStack implements Deck {
	
	private static DeckImpl singletonInstance;
	
	private DeckImpl() {
		populateDeck();
	}
	
 	public static DeckImpl getSingletonInstance(){
 	    if(singletonInstance == null)
            singletonInstance = new DeckImpl();

        return singletonInstance;
 	}


 	@Override
	public void populateDeck(){
		int deckSize = BoardImpl.getSingletonInstance().getBoardSize() * GameConstants.DECK_SIZE_MODIFIER;
		int numActionCards = deckSize / GameConstants.RATIO_OF_PATH_CARDS_TO_NON_PATH_CARDS;
		int numPersonalCards = deckSize / GameConstants.RATIO_OF_PATH_CARDS_TO_NON_PATH_CARDS;
				
		for (int i = 0; i < numActionCards; i++) {
			addCard(new CardImpl(ActionCardFactory.getSingletonInstance().createCardType(ActionCardType.values()[i % ActionCardType.values().length].toString())));
		}
		
		for (int i = 0; i < numPersonalCards; i++) {
			addCard(new CardImpl(PersonalCardFactory.getSingletonInstance().createCardType(PersonalCardType.values()[i % PersonalCardType.values().length].toString())));
		}
						
		for (int i = 0; i < (deckSize - numActionCards - numPersonalCards); i++){
			addCard(new CardImpl(PathCardFactory.getSingletonInstance().createCardType(PlayablePathCardType.values()[i % PlayablePathCardType.values().length].toString())));
		}
			
	}
		
	@Override
	public void shuffleDeck(){
		Collections.shuffle(super.getCards());
	}

}
