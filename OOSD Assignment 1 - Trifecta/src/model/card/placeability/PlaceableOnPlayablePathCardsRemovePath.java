package model.card.placeability;

import model.BoardImpl;
import model.TurnManagerImpl;
import model.cardcollection.CardStack;
import model.exception.PlaceabilityException;
import model.interfaces.Card;
import model.interfaces.CardPlaceability;
import model.interfaces.GameConstants.PlayablePathCardType;

/**
 * Class representing a card's placeability of being able to be placed on a playable path card 
 * and them removing the path card from the square
 * 
 * @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */
public class PlaceableOnPlayablePathCardsRemovePath implements CardPlaceability {
	
	public boolean placeCardOnSquare(CardStack cardStack, Card card, int row, int column) throws PlaceabilityException {
		
		boolean placeable = false;
		
		//if cardstack on square is not empty
		if(!cardStack.getCards().isEmpty()){
			
			//check card on top of stack
			Card cardOnTopOfCardStack = cardStack.getCards().peek();
			
			//if card on top of stack is a playable path card set placeable to true
			for(PlayablePathCardType type: PlayablePathCardType.values()){
				if(cardOnTopOfCardStack.getCardType().getId().equals(type.toString()))
					placeable = true;	
			}
		
			if(placeable == true){
				//if card can be placed remove path card from square and discard the card being placed
				BoardImpl.getSingletonInstance().removeCardFromSquare(row, column, cardOnTopOfCardStack);
				TurnManagerImpl.getSingletonInstance().playerDiscardCard();
			}
		} if(placeable == false){
			throw new PlaceabilityException();
		}
		
		return false;
	}

}
