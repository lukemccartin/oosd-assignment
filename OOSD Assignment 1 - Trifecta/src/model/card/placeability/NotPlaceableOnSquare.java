package model.card.placeability;

import model.cardcollection.CardStack;
import model.exception.PlaceabilityException;
import model.interfaces.Card;
import model.interfaces.CardPlaceability;

/**
 * Class representing a card's placeability of not being able to be placed on a square.
 * 
 * @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */
public class NotPlaceableOnSquare implements CardPlaceability{
	
	public boolean placeCardOnSquare(CardStack cardStack, Card card, int row, int column) throws PlaceabilityException{
		
		throw new PlaceabilityException();
		
	}
}
