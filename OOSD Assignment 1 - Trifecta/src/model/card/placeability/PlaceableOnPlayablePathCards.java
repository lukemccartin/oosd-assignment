package model.card.placeability;

import model.cardcollection.CardStack;
import model.exception.PlaceabilityException;
import model.interfaces.Card;
import model.interfaces.CardPlaceability;
import model.interfaces.GameConstants.PlayablePathCardType;

/**
 * Class representing a card's placeability of being able to be placed on a playable path card 
 * (i.e not a goal card or start card) 
 * 
 * @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */
public class PlaceableOnPlayablePathCards implements CardPlaceability {
	
	public boolean placeCardOnSquare(CardStack cardStack, Card card, int row, int column) throws PlaceabilityException {
		
		boolean placeable = false;
		
		//if cardstack on square is not empty
		if(!cardStack.getCards().isEmpty()){
			//check card on top of stack
			Card cardOnTopOfCardStack = cardStack.getCards().peek();
			
			//if card on top of stack is a playable path card set placeable to true
			for(PlayablePathCardType type: PlayablePathCardType.values()){
				if(cardOnTopOfCardStack.getCardType().getId().equals(type.toString()))
					placeable = true;	
			}
		
		}
		if(placeable == false){
			throw new PlaceabilityException();
		}
		
		return placeable;
	}

}
