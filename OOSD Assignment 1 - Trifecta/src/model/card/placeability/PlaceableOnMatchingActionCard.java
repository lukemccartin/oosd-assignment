package model.card.placeability;

import model.BoardImpl;
import model.TurnManagerImpl;
import model.card.actioncard.AbstractActionCard;
import model.cardcollection.CardStack;
import model.exception.PlaceabilityException;
import model.interfaces.Card;
import model.interfaces.CardPlaceability;

/**
 * Class representing a card's placeability of being able to be placed on it's matching action card
 * 
 * @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */
public class PlaceableOnMatchingActionCard implements CardPlaceability {
	
	public boolean placeCardOnSquare(CardStack cardStack, Card card, int row, int column) throws PlaceabilityException{
		
		boolean placeable = false;
		
		//if cardstack on square is not empty
		if(!cardStack.getCards().isEmpty()){
			//check card on top of stack
			Card cardOnTopOfCardStack = cardStack.getCards().peek();
			String matchingCardId = ((AbstractActionCard) card.getCardType()).getMatchingCardId();
			
			//if card on top of stack is this cards matching card
			if(cardOnTopOfCardStack.getCardType().getId().equals(matchingCardId)){
				//remove the card on top of the stack
				BoardImpl.getSingletonInstance().removeCardFromSquare(row, column, cardOnTopOfCardStack);
				
				//discard the card being placed
				TurnManagerImpl.getSingletonInstance().playerDiscardCard();
				placeable = true;
			}
		} 
		if(placeable == false){
			throw new PlaceabilityException();
		}
		return false;
	}

}
