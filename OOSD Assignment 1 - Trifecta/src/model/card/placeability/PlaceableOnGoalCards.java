package model.card.placeability;

import model.BoardImpl;
import model.TurnManagerImpl;
import model.cardcollection.CardStack;
import model.exception.PlaceabilityException;
import model.interfaces.Card;
import model.interfaces.CardPlaceability;

/**
 * Class representing a card's placeability of being able to be placed on goal cards
 * 
 * @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */
public class PlaceableOnGoalCards implements CardPlaceability {
	
	@Override
	public boolean placeCardOnSquare(CardStack cardStack, Card card, int row, int column) throws PlaceabilityException{
		
		boolean placeable = false;
		
		//if cardstack on square is not empty
		if(!cardStack.getCards().isEmpty()){
			//check card on top of stack
			Card cardOnTopOfCardStack = cardStack.getCards().peek();
			String cardId = cardOnTopOfCardStack.getCardType().getId();
			
			//if card on top of stack is a goal card
			if(cardId.equals("trueGoalPathCard") || cardId.equals("falseGoalPathCard"))
				
				//flip over the goal card
				BoardImpl.getSingletonInstance().flipCard(row, column);
				//discard the card being placed
				TurnManagerImpl.getSingletonInstance().playerDiscardCard();
				placeable = true;	
			}
		if(placeable == false){
			throw new PlaceabilityException();
		}
		
		return false;
	}

}