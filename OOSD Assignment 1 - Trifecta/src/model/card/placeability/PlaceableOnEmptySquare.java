package model.card.placeability;

import model.BoardSquareNeighbourCheckerImpl;
import model.cardcollection.CardStack;
import model.exception.PlaceabilityException;
import model.interfaces.Card;
import model.interfaces.CardPlaceability;

/**
 * Class representing a card's placeability of being able to be placed on an empty square.
 * 
 * @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */
public class PlaceableOnEmptySquare implements CardPlaceability {
	
	@Override
	public boolean placeCardOnSquare(CardStack cardStack, Card card, int row, int column) throws PlaceabilityException{

		boolean placeable = false;
		
		if(cardStack.getCards().isEmpty()){
			//check if the neighbouring card on the board is a goal type card
			if (BoardSquareNeighbourCheckerImpl.getSingletonInstance().checkIfAnyNeighbourIsGoalTypeCard(card, row, column) == true){
				//check if placing the card next to the goal is allowable e.g. is or will the goal card be connected to an existing path
				if(BoardSquareNeighbourCheckerImpl.getSingletonInstance().checkIfCardCanBePlacedNextToGoalCard(card, row, column) == true){
					placeable = true;
				}
			}
			//check if the neighbouring card has accessible paths that match the card the player is attempting to place
			else if (BoardSquareNeighbourCheckerImpl.getSingletonInstance().checkIfNeighbourIsAccessible(card, row, column)){
				placeable = true;
			}
		}
		if(placeable == false){
			throw new PlaceabilityException();
		}
		return placeable;
	}
}