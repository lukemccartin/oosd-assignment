package model.card;

import java.util.HashMap;

import model.interfaces.CardGiveability;
import model.interfaces.CardPlaceability;
import model.interfaces.CardType;

/**
 * Abstract class representing a card's type.
 * 
 * @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */
public abstract class AbstractCardType implements CardType{

	private String id;
	private double currentRotationAxis;
	private CardPlaceability cardPlaceability;
	private CardGiveability cardGiveability;
	protected HashMap<String, Boolean> edges;
	
	public AbstractCardType(String id) {
		this.id = id;
		edges = new HashMap<String, Boolean>();
		edges.put("top", false);
		edges.put("right", false);
		edges.put("bottom", false);
		edges.put("left", false);
		currentRotationAxis = 0;
	}

	@Override
	public String getId(){
		return id;
	}
	
	@Override
	public void setId(String id){
		this.id = id;
	}

	@Override
	public CardPlaceability getCardPlaceability() {
		return cardPlaceability;
	}
	
	@Override
	public void setPlaceability(CardPlaceability cardPlaceability) {
		this.cardPlaceability = cardPlaceability;
	}
	
	@Override
	public CardGiveability getCardGiveability() {
		return cardGiveability;
	}

	@Override
	public void setGiveability(CardGiveability cardGiveability) {
		this.cardGiveability =  cardGiveability;
	}

	public HashMap<String, Boolean> getEdges() {
		return edges;
	}

	public void setEdges(HashMap<String, Boolean> edges) {
		this.edges = edges;
	}
	
	public boolean isEdgeAccessible(String edge) {
		return edges.get(edge);
	}

	public void setEdgeAccessible(String edge, boolean edgeAccessible) {
		this.edges.put(edge, edgeAccessible);
	}

	@Override
	public void rotateCard90Degrees(){
		HashMap<String, Boolean> rotatedEdges = new HashMap<String, Boolean>();
		rotatedEdges.put("top", edges.get("left"));
		rotatedEdges.put("right", edges.get("top"));
		rotatedEdges.put("bottom", edges.get("right"));
		rotatedEdges.put("left", edges.get("bottom"));
		
		edges = rotatedEdges;
		currentRotationAxis += 90;
		if(currentRotationAxis == 360) {
			currentRotationAxis = 0;
		}
	}
	
	@Override
	public double getCurrentRotationAxis() {
		return currentRotationAxis;
	}

}
