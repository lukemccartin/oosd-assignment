package model.card.cardfactory;

import model.card.personalcard.ChippedToothPersonalCard;
import model.card.personalcard.HealedPawPersonalCard;
import model.card.personalcard.HealedToothPersonalCard;
import model.card.personalcard.HurtPawPersonalCard;
import model.card.personalcard.ImmunityPersonalCard;
import model.card.personalcard.SkipTurnPersonalCard;
import model.interfaces.CardType;

/**
 * Class representing the concrete factory which creates personal cards
 * @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */
public class PersonalCardFactory extends AbstractCardTypeFactory{
	
	private static PersonalCardFactory singletonInstance;
	
	private PersonalCardFactory(){
		
	}
	
 	public static AbstractCardTypeFactory getSingletonInstance(){
 	    if(singletonInstance == null)
            singletonInstance = new PersonalCardFactory();

        return singletonInstance;
 	}

	@Override
	public CardType createCardType(String cardTypeId) {
		
		switch(cardTypeId){
			case "chippedToothPersonalCard":
				return new ChippedToothPersonalCard(cardTypeId, "healedToothPersonalCard");
			case "hurtPawPersonalCard":
				return new HurtPawPersonalCard(cardTypeId, "healedPawPersonalCard");
			case "healedToothPersonalCard":
				return new HealedToothPersonalCard(cardTypeId, "chippedToothPersonalCard");
			case "healedPawPersonalCard":
				return new HealedPawPersonalCard(cardTypeId, "hurtPawPersonalCard");
			case "skipTurnPersonalCard":
				return new SkipTurnPersonalCard(cardTypeId, null);
			case "immunityPersonalCard":
				return new ImmunityPersonalCard(cardTypeId, null);
			default:
				return null;
		}
	
	}

}
