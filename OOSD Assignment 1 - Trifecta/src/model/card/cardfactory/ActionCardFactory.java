package model.card.cardfactory;

import model.card.actioncard.ClearPathActionCard;
import model.card.actioncard.GoalRevealActionCard;
import model.card.actioncard.LandfillActionCard;
import model.card.actioncard.LandfillRemovalActionCard;
import model.card.actioncard.LogBlockActionCard;
import model.card.actioncard.LogBlockRemovalActionCard;
import model.interfaces.CardType;

/**
 * Class representing the concrete factory which creates action cards
 * @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */
public class ActionCardFactory extends AbstractCardTypeFactory{
	
	private static ActionCardFactory singletonInstance;
	
	private ActionCardFactory(){
		
	}
	
 	public static AbstractCardTypeFactory getSingletonInstance(){
 	    if(singletonInstance == null)
            singletonInstance = new ActionCardFactory();

        return singletonInstance;
 	}

	@Override
	public CardType createCardType(String cardTypeId) {
		
		switch(cardTypeId){
			case "clearPathActionCard":
				return new ClearPathActionCard(cardTypeId, null);
			case "goalRevealActionCard":
				return new GoalRevealActionCard(cardTypeId, null);
			case "landfillActionCard":
				return new LandfillActionCard(cardTypeId, "landfillRemovalActionCard");
			case "landfillRemovalActionCard":
				return new LandfillRemovalActionCard(cardTypeId, "landfillActionCard");
			case "logBlockActionCard":
				return new LogBlockActionCard(cardTypeId, "logBlockRemovalActionCard");
			case "logBlockRemovalActionCard":
				return new LogBlockRemovalActionCard(cardTypeId, "logBlockActionCard");
			default:
				return null;
		}
	
	}

}
