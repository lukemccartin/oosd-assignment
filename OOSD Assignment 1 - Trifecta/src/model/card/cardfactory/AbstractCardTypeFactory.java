package model.card.cardfactory;

import model.interfaces.CardType;

/**
 * Class representing the abstract factory which delegates to concrete factories to create cards
 * @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */
public abstract class AbstractCardTypeFactory {

	public abstract CardType createCardType(String cardTypeId);

}
