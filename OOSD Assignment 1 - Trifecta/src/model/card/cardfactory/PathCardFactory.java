package model.card.cardfactory;

import model.card.pathcard.DeadEndPathCard;
import model.card.pathcard.FalseGoalPathCard;
import model.card.pathcard.FourWayIntersectPathCard;
import model.card.pathcard.LeftTurnPathCard;
import model.card.pathcard.RightTurnPathCard;
import model.card.pathcard.StartPathCard;
import model.card.pathcard.StraightPathCard;
import model.card.pathcard.TIntersectPathCard;
import model.card.pathcard.TrueGoalPathCard;
import model.interfaces.CardType;

/**
 * Class representing the concrete factory which creates path cards
 * @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */
public class PathCardFactory extends AbstractCardTypeFactory{

	private static PathCardFactory singletonInstance;
	
	private PathCardFactory(){
		
	}
	
 	public static AbstractCardTypeFactory getSingletonInstance(){
 	    if(singletonInstance == null)
            singletonInstance = new PathCardFactory();

        return singletonInstance;
 	}
	
	@Override
	public CardType createCardType(String cardTypeId) {
		
		switch(cardTypeId){
			case "tIntersectPathCard":
				return new TIntersectPathCard(cardTypeId);
			case "straightPathCard":
				return new StraightPathCard(cardTypeId);
			case "rightTurnPathCard":
				return new RightTurnPathCard(cardTypeId);
			case "leftTurnPathCard":
				return new LeftTurnPathCard(cardTypeId);
			case "fourWayIntersectPathCard":
				return new FourWayIntersectPathCard(cardTypeId);
			case "deadEndPathCard":
				return new DeadEndPathCard(cardTypeId);
			case "startPathCard":
				return new StartPathCard(cardTypeId);
			case "falseGoalPathCard":
				return new FalseGoalPathCard(cardTypeId);
			case "trueGoalPathCard":
				return new TrueGoalPathCard(cardTypeId);
			default:
				return null;
		}
	
	}

}
