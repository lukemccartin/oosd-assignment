package model.card.giveability;

import java.util.HashMap;

import model.card.personalcard.AbstractPersonalCard;
import model.exception.GiveabilityException;
import model.interfaces.Card;
import model.interfaces.CardGiveability;
import model.interfaces.Player;

/**
 * Class representing a card's giveability of being able to be given to a player
 * if that player has already applied with the cards matching effect.
 * 
 * @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */
public class GiveableToPlayerWithMatchingEffect implements CardGiveability {

	@Override
	public void giveCardToPlayer(Player player, Card card) throws GiveabilityException{

		HashMap <String, String> currentEffectsAppliedToPlayer = player.getPlayerType().getListOfEffectsAppliedToPlayer();
		
		if(!currentEffectsAppliedToPlayer.containsKey(((AbstractPersonalCard) card.getCardType()).getMatchingCardId()))
			throw new GiveabilityException("This card can only remove matching effect.");

	}
	
}
