package model.card.giveability;

import model.exception.GiveabilityException;
import model.interfaces.Card;
import model.interfaces.CardGiveability;
import model.interfaces.Player;

/**
 * Class representing a card's giveability of not being able to be given to a player.
 * 
 * @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */
public class NotGiveableToPlayer implements CardGiveability {

	@Override
	public void giveCardToPlayer(Player player, Card card) throws GiveabilityException{
		throw new GiveabilityException("Only personal cards can be given to players.");
	}
	
}
