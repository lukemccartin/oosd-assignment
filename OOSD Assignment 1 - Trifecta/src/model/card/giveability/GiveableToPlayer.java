package model.card.giveability;

import java.util.HashMap;

import model.exception.GiveabilityException;
import model.interfaces.Card;
import model.interfaces.CardGiveability;
import model.interfaces.Player;

/**
 * Class representing a card's giveability of being able to be given to a player
 * if that player has not been already applied with the same effect.
 * 
 * @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */
public class GiveableToPlayer implements CardGiveability {

	@Override
	public void giveCardToPlayer(Player player, Card card) throws GiveabilityException {
		
		HashMap <String, String> currentEffectsAppliedToPlayer = player.getPlayerType().getListOfEffectsAppliedToPlayer();
		
		if(currentEffectsAppliedToPlayer.containsKey(card.getCardType().getId()))
			throw new GiveabilityException("The same effect cannot be applied twice to a player.");
		
	}
	
}
