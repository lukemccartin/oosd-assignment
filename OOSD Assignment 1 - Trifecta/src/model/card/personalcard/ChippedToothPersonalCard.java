package model.card.personalcard;

import model.card.giveability.GiveableToPlayer;

/**
 * Class representing a chipped tooth personal card type.
 * 
 * @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */
public class ChippedToothPersonalCard extends AbstractPersonalCard {

	public ChippedToothPersonalCard(String id, String matchingCardId) {
		super(id, matchingCardId);
		setGiveability(new GiveableToPlayer());
	}
	
}
