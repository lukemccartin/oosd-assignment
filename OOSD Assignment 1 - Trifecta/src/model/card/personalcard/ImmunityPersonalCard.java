package model.card.personalcard;

import model.card.giveability.GiveableToPlayer;

/**
 * Class representing an immunity personal card type.
 * 
 * @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */
public class ImmunityPersonalCard extends AbstractPersonalCard {

	public ImmunityPersonalCard(String id, String matchingCardId) {
		super(id, matchingCardId);
		setGiveability(new GiveableToPlayer());
	}
	
}
