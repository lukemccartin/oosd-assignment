package model.card.personalcard;

import model.card.giveability.GiveableToPlayerWithMatchingEffect;

/**
 * Class representing a healed paw personal card type.
 * 
 * @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */

public class HealedPawPersonalCard extends AbstractPersonalCard {

	public HealedPawPersonalCard(String id, String matchingCardId) {
		super(id, matchingCardId);
		setGiveability(new GiveableToPlayerWithMatchingEffect());
	}

}
