package model.card.personalcard;

import model.card.giveability.GiveableToPlayer;

/**
 * Class representing a slip turn personal card type.
 * 
 * @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */
public class SkipTurnPersonalCard extends AbstractPersonalCard {

	public SkipTurnPersonalCard(String id, String matchingCardId) {
		super(id, matchingCardId);
		setGiveability(new GiveableToPlayer());
	}
	
}
