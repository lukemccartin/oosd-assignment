package model.card.personalcard;

import model.card.giveability.GiveableToPlayerWithMatchingEffect;

/**
 * Class representing a healed tooth personal card type.
 * 
 * @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */
public class HealedToothPersonalCard extends AbstractPersonalCard {

	public HealedToothPersonalCard(String id, String matchingCardId) {
		super(id, matchingCardId);
		setGiveability(new GiveableToPlayerWithMatchingEffect());
	}

}
