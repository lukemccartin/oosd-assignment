package model.card.personalcard;

import model.card.AbstractCardType;
import model.card.placeability.NotPlaceableOnSquare;

/**
 * Class representing a personal card type.
 * 
 * @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */
public abstract class AbstractPersonalCard  extends AbstractCardType{

	private String matchingCardId;
	
	public AbstractPersonalCard(String id, String matchingCardId) {
		super(id);
		this.matchingCardId = matchingCardId;
		setPlaceability(new NotPlaceableOnSquare());
	}
	
	public String getMatchingCardId(){
		return matchingCardId;
	}
	
	public void setMatchingCard(String matchingCardId){
		this.matchingCardId = matchingCardId;
	}

}
