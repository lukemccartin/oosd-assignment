package model.card.personalcard;

import model.card.giveability.GiveableToPlayer;

/**
 * Class representing a hurt paw personal card type.
 * 
 * @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */
public class HurtPawPersonalCard extends AbstractPersonalCard {

	public HurtPawPersonalCard(String id, String matchingCardId) {
		super(id, matchingCardId);
		setGiveability(new GiveableToPlayer());
	}

}
