package model.card;

import model.interfaces.Card;
import model.interfaces.CardType;

/**
 * Class representing a card.
 * 
 * @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */
public class CardImpl implements Card {

	private CardType cardType;
	private boolean isCardFacingDown = true;
	
	public CardImpl(CardType cardType) {
		this.cardType = cardType;
	}
	
	@Override
	public CardType getCardType() {
		return cardType;
	}
	
	@Override
	public void setCardType(CardType cardType){
		this.cardType = cardType;
	}

	@Override
	public boolean getIsCardFacingDown() {
		return isCardFacingDown;
	}

	@Override
	public void setIsCardFacingDown(boolean isCardFacingDown) {
		this.isCardFacingDown=isCardFacingDown;
		
	}
	
}
