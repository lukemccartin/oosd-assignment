package model.card.pathcard;

import model.card.AbstractCardType;
import model.card.giveability.NotGiveableToPlayer;
import model.card.placeability.PlaceableOnEmptySquare;


/**
 * Class representing a path card type.
 * 
 * @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */
public abstract class AbstractPathCard extends AbstractCardType{

	public AbstractPathCard(String id) {
		super(id);
		setPlaceability(new PlaceableOnEmptySquare());
		setGiveability(new NotGiveableToPlayer());
	}

	protected abstract void setDefaultEdgeAccessibility();

}
