package model.card.pathcard;
/**
 * Class representing a right turn path card type.
 * 
 * @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */
public class RightTurnPathCard extends AbstractPathCard {

	
	public RightTurnPathCard(String id) {
		super(id);
		setDefaultEdgeAccessibility();
	}
	

	@Override
	protected void setDefaultEdgeAccessibility() {
		edges.put("top", true);
		edges.put("right", false);
		edges.put("bottom", false);
		edges.put("left", true);
	}
}
