package model.card.pathcard;
/**
 * Class representing a t intersect path card type.
 * 
 * @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */
public class TIntersectPathCard extends AbstractPathCard {

	
	public TIntersectPathCard(String id) {
		super(id);
		setDefaultEdgeAccessibility();
	}

	@Override
	protected void setDefaultEdgeAccessibility() {
		edges.put("top", true);
		edges.put("right", true);
		edges.put("bottom", true);
		edges.put("left", false);
	}
}
