package model.card.pathcard;
/**
 * Class representing a left turn path card type.
 * 
 * @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */
public class LeftTurnPathCard extends AbstractPathCard {

	
	public LeftTurnPathCard(String id) {
		super(id);
		setDefaultEdgeAccessibility();
	}

	@Override
	protected void setDefaultEdgeAccessibility() {
		edges.put("top", true);
		edges.put("right", true);
		edges.put("bottom", false);
		edges.put("left", false);
	}
}
