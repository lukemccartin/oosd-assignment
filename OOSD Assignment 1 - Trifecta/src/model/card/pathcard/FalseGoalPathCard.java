package model.card.pathcard;
/**
 * Class representing a false goal path card type.
 * 
 * @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */
public class FalseGoalPathCard extends AbstractPathCard{

	
	public FalseGoalPathCard(String id) {
		super(id);
		setDefaultEdgeAccessibility();
	}

	@Override
	protected void setDefaultEdgeAccessibility() {
		edges.put("top", true);
		edges.put("right", true);
		edges.put("bottom", true);
		edges.put("left", true);
	}

}
