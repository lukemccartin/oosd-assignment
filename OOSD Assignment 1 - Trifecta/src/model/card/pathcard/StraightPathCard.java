package model.card.pathcard;
/**
 * Class representing a straight path card type.
 * 
 * @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */
public class StraightPathCard extends AbstractPathCard {

	
	public StraightPathCard(String id) {
		super(id);
		setDefaultEdgeAccessibility();
	}

	@Override
	protected void setDefaultEdgeAccessibility() {
		edges.put("top", true);
		edges.put("right", false);
		edges.put("bottom", true);
		edges.put("left", false);
	}

}
