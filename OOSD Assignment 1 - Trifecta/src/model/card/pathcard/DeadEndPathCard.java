package model.card.pathcard;
/**
 * Class representing a dead end path card type.
 * 
 * @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */
public class DeadEndPathCard extends AbstractPathCard {

	
	public DeadEndPathCard(String id) {
		super(id);
		setDefaultEdgeAccessibility();
	}

	@Override
	protected void setDefaultEdgeAccessibility() {
		edges.put("top", true);
		edges.put("right", false);
		edges.put("bottom", false);
		edges.put("left", false);
	}

}
