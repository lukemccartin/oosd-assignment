package model.card.actioncard;

import model.card.placeability.PlaceableOnPlayablePathCardsRemovePath;
/**
 * Class representing an clear path action card type.
 * 
 * @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */
public class ClearPathActionCard extends AbstractActionCard {

	public ClearPathActionCard(String id, String matchingCardId) {
		super(id, matchingCardId);
		setPlaceability(new PlaceableOnPlayablePathCardsRemovePath());
	}

}
