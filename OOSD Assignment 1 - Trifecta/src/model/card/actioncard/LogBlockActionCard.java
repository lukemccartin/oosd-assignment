package model.card.actioncard;

import model.card.placeability.PlaceableOnPlayablePathCards;
/**
 * Class representing a log block action card type.
 * 
 * @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */
public class LogBlockActionCard extends AbstractActionCard {

	public LogBlockActionCard(String id, String matchingCardId) {
		super(id, matchingCardId);
		setPlaceability(new PlaceableOnPlayablePathCards());
	}

}
