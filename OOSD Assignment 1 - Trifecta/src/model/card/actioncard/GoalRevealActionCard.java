package model.card.actioncard;

import model.card.placeability.PlaceableOnGoalCards;
/**
 * Class representing a goal reveal action card type.
 * 
 * @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */
public class GoalRevealActionCard extends AbstractActionCard {

	public GoalRevealActionCard(String id, String matchingCardId) {
		super(id, matchingCardId);
		setPlaceability(new PlaceableOnGoalCards());
	}

}
