package model.card.actioncard;

import model.card.placeability.PlaceableOnMatchingActionCard;
/**
 * Class representing a log block removal action card type.
 * 
 * @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */
public class LogBlockRemovalActionCard extends AbstractActionCard {

	public LogBlockRemovalActionCard(String id, String matchingCardId) {
		super(id, matchingCardId);
		setPlaceability(new PlaceableOnMatchingActionCard());
	}

}
