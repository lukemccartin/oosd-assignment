package model.card.actioncard;

import model.card.placeability.PlaceableOnMatchingActionCard;
/**
 * Class representing a landfill removal action card type.
 * 
 * @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */
public class LandfillRemovalActionCard extends AbstractActionCard {

	public LandfillRemovalActionCard(String id, String matchingCardId) {
		super(id, matchingCardId);
		setPlaceability(new PlaceableOnMatchingActionCard());
	}

}
