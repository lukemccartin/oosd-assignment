package model.card.actioncard;

import model.card.placeability.PlaceableOnPlayablePathCards;
/**
 * Class representing a landfill action card type.
 * 
 * @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */
public class LandfillActionCard extends AbstractActionCard {

	public LandfillActionCard(String id, String matchingCardId) {
		super(id, matchingCardId);
		setPlaceability(new PlaceableOnPlayablePathCards());
	}

}
