package model.card.actioncard;

import model.card.AbstractCardType;
import model.card.giveability.NotGiveableToPlayer;

/**
 * Class representing an action card type.
 * 
 * @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */
public abstract class AbstractActionCard extends AbstractCardType {
	
	private String matchingCardId;
	
	public AbstractActionCard(String id, String matchingCardId) {
		super(id);
		this.matchingCardId = matchingCardId;
		setGiveability(new NotGiveableToPlayer());
	}
	
	public String getMatchingCardId(){
		return matchingCardId;
	}
	
	public void setMatchingCard(String matchingCardId){
		this.matchingCardId = matchingCardId;
	}

}
