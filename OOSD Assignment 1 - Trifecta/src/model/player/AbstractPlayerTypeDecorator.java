package model.player;

import java.util.HashMap;
import model.exception.PlaceabilityException;
import model.interfaces.PlayerType;

/**
 * Class representing the abstract player type decorator.
 * 
 * @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */
public abstract class AbstractPlayerTypeDecorator implements PlayerType {

	//Reference to the original playerType that has been decorated.
	protected PlayerType playerType;

	public AbstractPlayerTypeDecorator(PlayerType playerType) {
		this.playerType = playerType;
	}
	
	/**
	 * The generic method that all decorator classes implement with their
	 * own specific effects that affect how a player acts within the game.
	 * @param playerType - a reference to the wrapped object, calls its
	 * custom implementation of the same method to ensure all effects checked.
	 */
	@Override
	public void checkForDecoratedEffect() throws PlaceabilityException{
		playerType.checkForDecoratedEffect();
	}
	
	@Override
	public HashMap<String, String> getListOfEffectsAppliedToPlayer() {
		return playerType.getListOfEffectsAppliedToPlayer();
	}
	
	@Override
	public void addEffect(String playerString, String message) {
		this.playerType.addEffect(playerString, message);
	}

	@Override
	public void removeEffect(String playerTypeId) {
		playerType.removeEffect(playerTypeId);
	}

	@Override
	public PlayerType getBasePlayerType() {
		return playerType.getBasePlayerType();
	}

	@Override
	public String toString(){
		return playerType.toString();
	}
}
