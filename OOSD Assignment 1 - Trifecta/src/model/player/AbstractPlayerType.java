package model.player;

import java.util.HashMap;
import model.exception.PlaceabilityException;
import model.interfaces.PlayerType;

/**
 * Class representing the abstract player type.
 * 
 * @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */
public abstract class AbstractPlayerType implements PlayerType {


	private HashMap<String, String> currentEffectsAppliedToPlayer;
	
	public AbstractPlayerType() {
		currentEffectsAppliedToPlayer = new HashMap<String, String>();
	}

	@Override
	public void checkForDecoratedEffect() throws PlaceabilityException {
		//no effect so do nothing
	}

	@Override
	public HashMap<String, String> getListOfEffectsAppliedToPlayer() {
		return currentEffectsAppliedToPlayer;
	}

	@Override
	public void addEffect(String effectToPlayer, String message) {
		currentEffectsAppliedToPlayer.put(effectToPlayer, message);
	}

	@Override
	public void removeEffect(String playerTypeId) {
		if(currentEffectsAppliedToPlayer.containsKey(playerTypeId)){
			currentEffectsAppliedToPlayer.remove(playerTypeId);
		}	
	}

	/**
	 * Allows the original playerType - of either Worker or Sabouter,
	 * to be returned.
	 * @return PlayerType.
	 */
	@Override
	public PlayerType getBasePlayerType() {
		return this;
	}
}
