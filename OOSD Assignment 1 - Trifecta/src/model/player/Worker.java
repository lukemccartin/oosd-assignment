package model.player;

/**
 * Class representing a worker player type.
 * 
 * @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */
public class Worker extends AbstractPlayerType {

	public String toString(){
		return "Worker";
	}

}
