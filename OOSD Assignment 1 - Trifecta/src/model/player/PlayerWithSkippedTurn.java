package model.player;

import java.util.List;
import model.TurnManagerImpl;
import model.exception.PlaceabilityException;
import model.interfaces.Player;
import model.interfaces.PlayerType;
import model.interfaces.TurnManager;

/**
 * Class representing the decorated skipped turn player.
 * 
 * @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */
public class PlayerWithSkippedTurn extends AbstractPlayerTypeDecorator {
	
	public PlayerWithSkippedTurn(String effectToPlayer, PlayerType playerType, String message) {
		super(playerType);
		addEffect(effectToPlayer, message);
	}

	/*
	 * Skipped turn effect - this effect is only called once, at the start of a the affected
	 * players turn.
	 * thisPlayer - used for removing this effect from the player when this method isc
	 * called to ensure only one turn is skipped.
	 * allPlayers - used to loop through current players and assign the current turn
	 * to the next player
	 */
	@Override
	public void checkForDecoratedEffect() throws PlaceabilityException {
		super.checkForDecoratedEffect();
		
		TurnManager turnManager = TurnManagerImpl.getSingletonInstance();
		Player thisPlayer = turnManager.getCurrentPlayer();
		List<Player> allPlayers = turnManager.getAllPlayers();
		PlayerTypeDecoratorFactory.getSingletonInstance().buildDecoratedPlayer("selfRemoveSkipTurn", thisPlayer);
		
		turnManager.notifyForPaneChange(thisPlayer);
		
		//if not the last player, set the player to the next higher player number.
		if (thisPlayer.getPlayerId() < allPlayers.size()){
			turnManager.setCurrentPlayer(allPlayers.get(thisPlayer.getPlayerId()));	
		
		}
		//if last player in rotation, set to the first player
		else {
			turnManager.setCurrentPlayer(allPlayers.get(0));
		}
		
	}
	
}
