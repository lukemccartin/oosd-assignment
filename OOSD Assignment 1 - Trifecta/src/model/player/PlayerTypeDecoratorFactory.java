package model.player;

import java.util.HashMap;
import model.interfaces.Player;
import model.interfaces.PlayerType;

/**
 * Class representing the decorator factory.
 * 
 * @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */
public class PlayerTypeDecoratorFactory {
	
private static PlayerTypeDecoratorFactory singletonInstance;
	
	private PlayerTypeDecoratorFactory(){
		
	}
	
 	public static PlayerTypeDecoratorFactory getSingletonInstance(){
 	    if(singletonInstance == null)
            singletonInstance = new PlayerTypeDecoratorFactory();

        return singletonInstance;
 	}
	
	public PlayerType buildDecoratedPlayer(String cardId, Player player) {
		
		switch(cardId) {
			case "chippedToothPersonalCard":
				return new PlayerWithChippedTooth(cardId, player.getPlayerType(), "Chipped Tooth");
			case "hurtPawPersonalCard":
				return new PlayerWithHurtPaw(cardId, player.getPlayerType(), "Hurt Paw");
			case "healedToothPersonalCard":
				return handleRemovalOfDecoration("chippedToothPersonalCard", player);
			case "healedPawPersonalCard":
				return handleRemovalOfDecoration("hurtPawPersonalCard", player);
			case "skipTurnPersonalCard":
				return new PlayerWithSkippedTurn(cardId, player.getPlayerType(), "Skipped");
			case "selfRemoveSkipTurn":
				return handleRemovalOfDecoration("skipTurnPersonalCard", player);
			case "immunityPersonalCard":
				handleRemovalOfDecoration("skipTurnPersonalCard", player);
				handleRemovalOfDecoration("hurtPawPersonalCard", player);
				handleRemovalOfDecoration("chippedToothPersonalCard", player);
				PlayerType newPlayerType = new PlayerWithImmunity(cardId, player.getPlayerType(), "Immune");
				return newPlayerType;
			case "selfRemoveImmunity":
				return handleRemovalOfDecoration("immunityPersonalCard", player);
			default:
				return null;
		}
	}
	
	/*
	 * Handles removal of appropriate decoration when needed, then rebuilds the player with
	 * the decorations required to remain.
	 */
	private PlayerType handleRemovalOfDecoration(String effectToPlayer, Player player){
		
		//if effect is applied, remove from list of effects held by player.
		if(player.getPlayerType().getListOfEffectsAppliedToPlayer().containsKey(effectToPlayer)){
			player.getPlayerType().removeEffect(effectToPlayer);
			HashMap <String, String> currentEffectsAppliedToPlayer = player.getPlayerType().getListOfEffectsAppliedToPlayer();
			
			PlayerType basePlayerType = player.getPlayerType().getBasePlayerType();
			player.setPlayerType(basePlayerType);
			
			//loops through all effects remaining and rebuilds playertype
			for(String effectString: currentEffectsAppliedToPlayer.keySet()){
				player.setPlayerType(buildDecoratedPlayer(effectString, player));
			}
		}
		
		return player.getPlayerType();
	}
}
