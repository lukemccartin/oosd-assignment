package model.player;

import model.TurnManagerImpl;
import model.card.pathcard.AbstractPathCard;
import model.exception.PlaceabilityException;
import model.interfaces.Card;
import model.interfaces.PlayerType;

/**
 * Class representing the decorated hurt paw player.
 * 
 * @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */
public class PlayerWithHurtPaw extends AbstractPlayerTypeDecorator {

	public PlayerWithHurtPaw(String effectToPlayer, PlayerType playerType, String message) {
		super(playerType);
		addEffect(effectToPlayer, message);
	}

	//Hurt paw effect implementation
	@Override
	public void checkForDecoratedEffect() throws PlaceabilityException{
		super.checkForDecoratedEffect();
		
		//Checks card being placed by player, if path card, disallows placement
		Card currentCard = TurnManagerImpl.getSingletonInstance().getCardSelected();
		if(currentCard != null){
			if(currentCard.getCardType() instanceof AbstractPathCard){
				throw new PlaceabilityException("Player cannot play that card due to hurt paw.");
			}
		}
	}
}
