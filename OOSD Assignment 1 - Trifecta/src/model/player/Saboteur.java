package model.player;

/**
 * Class representing a saboteur player type.
 * 
 * @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */
public class Saboteur extends AbstractPlayerType{
	
	public String toString(){
		return "Saboteur";
	}

}
