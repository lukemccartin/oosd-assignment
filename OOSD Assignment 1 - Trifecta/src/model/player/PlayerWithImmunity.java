package model.player;

import model.TurnManagerImpl;
import model.card.giveability.GiveableToPlayer;
import model.exception.PlaceabilityException;
import model.interfaces.Card;
import model.interfaces.Player;
import model.interfaces.PlayerType;

/**
 * Class representing the decorated immune player.
 * 
 * @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */
public class PlayerWithImmunity extends AbstractPlayerTypeDecorator {
	
	int numberOfTurnsImmune;
	
	public PlayerWithImmunity(String effectToPlayer, PlayerType playerType, String message) {
		super(playerType);
		addEffect(effectToPlayer, message);
		numberOfTurnsImmune = 2;
	}

	//Immunity effect implementation.
	@Override
	public void checkForDecoratedEffect() throws PlaceabilityException {
		super.checkForDecoratedEffect();
		
		/*
		 * checks that card being placed on player is of correct type, if so
		 * and has remaining immune turns, stops card from being played onto
		 * player
		 */
		Card currentCard = TurnManagerImpl.getSingletonInstance().getCardSelected();
		if(currentCard != null && numberOfTurnsImmune > 0 ){
			if(currentCard.getCardType().getCardGiveability() instanceof GiveableToPlayer){
				throw new PlaceabilityException("Player is Immune to Personal Effects for " + numberOfTurnsImmune + " more turn cycles!");
			}
		} else {
			/*
			 * Decrements by 1 each turn for the player, as effects are checked at 
			 * beginning of turn also this will always be called at the start of a 
			 * turn when the card selected is naturally null
			 */
			numberOfTurnsImmune--;
		}
		
		//Removes effect once 2 turns have passed
		if(numberOfTurnsImmune < 1) {
			Player thisPlayer = TurnManagerImpl.getSingletonInstance().getCurrentPlayer();
			PlayerTypeDecoratorFactory.getSingletonInstance().buildDecoratedPlayer("selfRemoveImmunity", thisPlayer);
			TurnManagerImpl.getSingletonInstance().notifyForPaneChange(thisPlayer);
		}
	}
}
