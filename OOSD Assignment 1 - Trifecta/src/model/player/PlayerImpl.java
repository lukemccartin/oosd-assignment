package model.player;

import java.util.ArrayList;
import java.util.List;

import model.interfaces.Card;

//NOTE FOR LIZ - make player type into subclass and use the fly thing to manage player types
//Also do start stage and images

import model.interfaces.Player;
import model.interfaces.PlayerType;

/**
 * Class representing the player.
 * 
 * @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */
public class PlayerImpl implements Player {

	private int playerId;
	private PlayerType playerType;
	private int playerScore;
	private List<Card> playerHand;
		
	public PlayerImpl(int playerId){
		this.playerId = playerId;
		this.playerScore = 0;
		this.playerType = null;
		playerHand = new ArrayList<Card>();
	}
	
	@Override
	public int getPlayerId() {
		return playerId;
	}

	@Override
	public void setPlayerId(int playerId) {
		this.playerId = playerId;

	}

	@Override
	public int getPlayerScore() {
		return playerScore;
	}

	@Override
	public void setPlayerScore(int points) {
		this.playerScore = this.playerScore + points;

	}

	@Override
	public List<Card> getPlayerHand() {
		return playerHand;
	}

	@Override
	public PlayerType getPlayerType() {
		return playerType;
	}
	
	@Override
	public void setPlayerType(PlayerType playerType) {
		this.playerType = playerType;
	}
}
