package model.player;

import model.TurnManagerImpl;
import model.card.actioncard.AbstractActionCard;
import model.exception.PlaceabilityException;
import model.interfaces.Card;
import model.interfaces.PlayerType;

/**
 * Class representing the decorated chipped tooth player.
 * 
 * @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */
public class PlayerWithChippedTooth extends AbstractPlayerTypeDecorator {
	
	public PlayerWithChippedTooth(String effectToPlayer, PlayerType playerType, String message) {
		super(playerType);
		addEffect(effectToPlayer, message);
	}

	//Chipped tooth implementation.
	@Override
	public void checkForDecoratedEffect() throws PlaceabilityException {
		super.checkForDecoratedEffect();
		Card currentCard = TurnManagerImpl.getSingletonInstance().getCardSelected();
		
		//Checks card being placed by player, if action card, disallows placement
		if(currentCard != null){
			if(currentCard.getCardType() instanceof AbstractActionCard)
				throw new PlaceabilityException("Player cannot play that card due to chipped tooth.");
		}
		
	}
	
}
