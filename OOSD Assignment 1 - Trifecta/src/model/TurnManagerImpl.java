package model;

import java.util.List;
import java.util.Observable;

import javafx.application.Platform;
import javafx.scene.control.Alert.AlertType;
import model.card.personalcard.AbstractPersonalCard;
import model.cardcollection.CardStack;
import model.cardcollection.DeckImpl;
import model.exception.GiveabilityException;
import model.exception.PlaceabilityException;
import model.interfaces.Board;
import model.interfaces.Card;
import model.interfaces.CardType;
import model.interfaces.Player;
import model.interfaces.TurnManager;
import model.player.PlayerTypeDecoratorFactory;
import view.utilities.AlertDialog;
import view.utilities.EndRoundAlert;

/**
 * Class representing the turn manager which handles each turn by setting the current player,
 * reseting the timer at the end of each turn, storing the card selected by a player during a turn,
 * and handling player actions such as place card on board, discard card and give card to another
 * player.
 * 
 * @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */
public class TurnManagerImpl extends Observable implements TurnManager {

	private static TurnManager singletonInstance;
	private CardStack discardPile;
	private List<Player> players;
	private Player currentPlayer;
	private Card cardSelected;
	private TurnTimerService turnTimerService;
	private int timeLimitForEachTurn;
	
	
	private TurnManagerImpl() {
		players = PlayerManagerImpl.getSingletonInstance().getAllPlayers();
		discardPile = GameEngineImpl.getSingletonInstance().getDiscardPile();
		turnTimerService = new TurnTimerService();
	}
	
 	public static TurnManager getSingletonInstance(){
 	    if(singletonInstance == null)
            singletonInstance = new TurnManagerImpl();

        return singletonInstance;
 	}

 	@Override
	public void setTimeLimitForEachTurn(int timeLimitForEachTurn) {
		this.timeLimitForEachTurn = timeLimitForEachTurn;
	}

	@Override
	public Player getCurrentPlayer(){
		return currentPlayer;
	}
	
	@Override
	public List<Player> getAllPlayers(){
		return players;
	}
	
	@Override
	public CardStack getDiscardPile(){
		return discardPile;
	}
	
	@Override
	public void setCurrentPlayer(Player player){
		currentPlayer = player;
		notifyObserversOfPlayerHandChange();
	}
	
	@Override
	public Card getCardSelected() {
		return cardSelected;
	}

	@Override
	public void setCardSelected(Card cardSelected) {
		this.cardSelected = cardSelected;
	}
	
	@Override
	public TurnTimerService getTurnTimerService() {
		return turnTimerService;
	}

	@Override
	public void startFirstTurn(){
		setCurrentPlayer(players.get(0));
		
		setTurnTimer();
	}
	
	@Override
	public void setTurnTimer() {
		if(timeLimitForEachTurn > 0) {
			turnTimerService.setSelectedTurnTimer(timeLimitForEachTurn);
			turnTimerService.setCountdownTimerForDisplay(timeLimitForEachTurn);
			
		}
	}
	
	@Override
	public void cancelCurrentTurnTimer(){
		turnTimerService.resetCountdownTimer();
		turnTimerService.cancelCurrentTurnTimer();
	}
	
	private void endRound(){
		cancelCurrentTurnTimer();
		RoundManagerImpl.getSingletonInstance().endRound();
		notifyObserversOfDiscardPileChange();
	}
	
	@Override
	public void endTurn(){

		/*
		 * Ensures card sides are rotated back to original state to comply with
		 * the square view they are contained within
		 */
		for(Card playerCard: currentPlayer.getPlayerHand()){
			while(playerCard.getCardType().getCurrentRotationAxis() != 0){
				playerCard.getCardType().rotateCard90Degrees();
			}
		}

		//if all player hands are empty then saboteurs win
		if(PlayerManagerImpl.getSingletonInstance().checkPlayerHandsAreEmpty()){
			cardSelected = null;  
			
			ScoreManagerImpl.getSingletonInstance().calculateScores("Saboteur", currentPlayer);
			
			//Platform.runLater used to ensure thread safety in relation to timer service.
			Platform.runLater(new Runnable() {
				  @Override public void run() {
					  new EndRoundAlert(AlertType.INFORMATION, "Saboteur");                    
				  }
			});
			endRound();		
		}
		
		else{
			cancelCurrentTurnTimer();
			cardSelected=null;
			
			try {
				//After setting the next player in turn, check their effects
				if (currentPlayer.getPlayerId() < players.size()){
					setCurrentPlayer(players.get(currentPlayer.getPlayerId()));				
				}
				else {
					setCurrentPlayer(players.get(0));
				}
				currentPlayer.getPlayerType().checkForDecoratedEffect();
				setTurnTimer();
			} catch (PlaceabilityException pe) {
				new AlertDialog(AlertType.ERROR , pe.getMessage());
			}
		}
	}
	
	@Override
	public boolean placeCardOnBoard(int row, int column) {
		
		boolean cardPlacedOnBoard = false;
		boolean pathCompleted = false;
		
		if(cardSelected != null){
			
			try{
				//check decorated effects to ensure player is not restricted from placing current cardType
				currentPlayer.getPlayerType().checkForDecoratedEffect();
				
				if(cardPlacedOnBoard = BoardImpl.getSingletonInstance().addCardToSquare(row, column, cardSelected)){
					currentPlayer.getPlayerHand().remove(cardSelected);		
				}
			} catch(PlaceabilityException pe){
				new AlertDialog(AlertType.ERROR , pe.getMessage());
			}

			pathCompleted = BoardPathCheckerImpl.getSingletonInstance().depthFirstSearch(BoardImpl.getSingletonInstance().getStartCoordinate(), BoardImpl.getSingletonInstance().getTrueGoalCoordinate());
			
			//if path from start to the goal is found then workers win
			if(pathCompleted){
				cancelCurrentTurnTimer();				
				ScoreManagerImpl.getSingletonInstance().calculateScores("Worker", currentPlayer);
				new EndRoundAlert(AlertType.INFORMATION, "Worker");	
				endRound();
			} else if(cardPlacedOnBoard) {
				DealerImpl.getSingletonInstance().dealCardToPlayer(currentPlayer);
				endTurn();
			
			}
		}
		return cardPlacedOnBoard;
	}

	@Override
	public void undoPlaceCardOnBoard(int row, int column, Player player){
		currentPlayer = player;
		Board board = BoardImpl.getSingletonInstance();
		Card lastCardAddedToSquare = board.getSquare(row, column).getCards().peek();
		CardType cardType = lastCardAddedToSquare.getCardType();
		
		while(cardType.getCurrentRotationAxis() != 0){
 			cardType.rotateCard90Degrees();
 		}
		
		board.removeCardFromSquare(row, column, lastCardAddedToSquare);
		removeCardLastAddedToPlayerHand();

		currentPlayer.getPlayerHand().add(lastCardAddedToSquare);
		notifyObserversOfPlayerHandChange();
		resetTimers();
	}
		
	@Override
	public boolean playerDiscardCard(){
		boolean cardDiscarded = false;
		
		if(cardSelected != null){
			discard(cardSelected);
			endTurn();
			cardDiscarded = true;
		}
		
		return cardDiscarded;
	}
	
	@Override
	public boolean forcedDiscard(){
		List<Card> currentPlayerHand = currentPlayer.getPlayerHand();
		Card card = currentPlayerHand.get(currentPlayerHand.size()-1);
		discard(card);
		endTurn();
		return true;
	}
	
	/**
	 * for a given card add the card to the discard pile and remove the card from the player's hand
	 * call the dealer to remove a card from the deck and add it to the player's hand
	 * 
	 * @param card
	 */
	private void discard(Card card){
		discardPile.addCard(card); 
		currentPlayer.getPlayerHand().remove(card); 
		notifyObserversOfDiscardPileChange();
		DealerImpl.getSingletonInstance().dealCardToPlayer(currentPlayer);
	}
	
	@Override
	public void undoDiscardCard(Player player){
		currentPlayer = player;
		Card card = discardPile.removeCard();
		CardType cardType = card.getCardType();
		
		while(cardType.getCurrentRotationAxis() != 0){
 			cardType.rotateCard90Degrees();
 		}
		
		removeCardLastAddedToPlayerHand();
		currentPlayer.getPlayerHand().add(card);
		notifyObserversOfPlayerHandChange();
		notifyObserversOfDiscardPileChange();
		resetTimers();
	}
	
	@Override
	public boolean giveCardToPlayer(int playerId) {

		boolean cardGivenToPlayer = false;
		Player toPlayer = null;
		
		if(cardSelected != null){
			
			CardType cardType = cardSelected.getCardType();
			
			//checks playerId's against the Id passed in to ensure correct placement
			for(Player player: players){
				if(player.getPlayerId() == playerId)
					toPlayer = player;
			}
		
			try{
				//Checks players decorated effects to ensure no effects are blocking card placement
				toPlayer.getPlayerType().checkForDecoratedEffect();
				//Checks card is a cardType that can be given to a player
				cardType.getCardGiveability().giveCardToPlayer(toPlayer, cardSelected);
				//decorates player with new effect according to card placed
				toPlayer.setPlayerType(PlayerTypeDecoratorFactory.getSingletonInstance().buildDecoratedPlayer(cardType.getId(), toPlayer));
				discard(cardSelected);
				cardGivenToPlayer = true;
				setChanged();
				notifyObservers(toPlayer);
				endTurn();
			} catch(GiveabilityException | PlaceabilityException ge){
				new AlertDialog(AlertType.ERROR , ge.getMessage());
			}
		}

		return cardGivenToPlayer;
	}

	@Override
	public void undoGiveCardToPlayer(Player fromPlayer, int toPlayerId) {
		Player toPlayer = null;
		currentPlayer = fromPlayer;
		
		for(Player player: players){
			if(player.getPlayerId() == toPlayerId)
				toPlayer = player;
		}
		
		//remove the last card added to the discard pile
		Card card = discardPile.removeCard();
		
		AbstractPersonalCard cardType = (AbstractPersonalCard) card.getCardType();
		
		//remove the decoration from the player who was given the card
		toPlayer.setPlayerType(PlayerTypeDecoratorFactory.getSingletonInstance().buildDecoratedPlayer(cardType.getMatchingCardId(), toPlayer));
		
		/*
		 * remove the card that was last added to the player who gave the card and add back the card that was ontop of
		 * the discard pile
		 */
		removeCardLastAddedToPlayerHand();
		currentPlayer.getPlayerHand().add(card);
		
		notifyObserversOfPlayerHandChange();
		notifyObserversOfDiscardPileChange();
		resetTimers();
	}
	
	private void removeCardLastAddedToPlayerHand(){
		List<Card> playerHand = currentPlayer.getPlayerHand();
		Card lastCardAddedToPlayerHand = playerHand.get(playerHand.size()-1);
		DeckImpl.getSingletonInstance().addCard(lastCardAddedToPlayerHand);
		playerHand.remove(lastCardAddedToPlayerHand);
	}
	
	private void notifyObserversOfPlayerHandChange(){
		setChanged();
		notifyObservers(currentPlayer);
	}
	
	private void notifyObserversOfDiscardPileChange(){
		setChanged();
		notifyObservers(discardPile);
	}

	@Override
	public void notifyForPaneChange(Player toPlayer) {
		setChanged();
		notifyObservers(toPlayer);
		
	}
	
	private void resetTimers(){
		cancelCurrentTurnTimer();
		setTurnTimer();
	}
	
}
