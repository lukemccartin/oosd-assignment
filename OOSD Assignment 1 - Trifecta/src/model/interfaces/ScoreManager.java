package model.interfaces;

import java.util.List;
import java.util.Observer;

/**
 * Interface representing the score manager which handles the calculation of player scores after each round
 * by evaluating the winning player type at the end of each round and distributing points to the appropriate
 * players of that player type.
 * 
 * @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */
public interface ScoreManager {

	/**
	 * given a player who ended the round and the type of players that won the round, distribute points to players
	 * @param winningPlayerType - the player type associated with the group of players that won the round
	 * @param player - the player who ended the round
	 */
	public abstract void calculateScores(String winningPlayerType, Player player);

	/**
	 * adds an observer
	 * @param o - the observer to add
	 */
	public abstract void addObserver(Observer o);

	/**
	 * @return returns a list of players with the highest points
	 */
	public abstract List<Player> getWinnersByPoints();

}
