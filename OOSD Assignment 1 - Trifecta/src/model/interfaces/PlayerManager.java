package model.interfaces;

import java.util.List;
import java.util.Observer;

/**
 * Interface representing the player manager which creates the players, allocates their type
 * and stores the players
 * 
 * @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */
public interface PlayerManager {

	/**
	 * create and add players to the game based on the number of
	 * players specified
	 * 
	 * @param numPlayers - number of players to be created
	 */
	public abstract void createPlayers(int numPlayers);

	/**
	 * assigns player type to player
	 */
	public abstract void assignPlayerTypeToPlayers();
	
	/**
	 * return a player for a given id
	 * 
	 * @param id - the player's id
	 * @return the player
	 */
	public abstract Player getPlayer(int id);

	/**
	 * @return a list of the players in the fame
	 */
	public abstract List<Player> getAllPlayers();
	
	/**
	 * resets the number of undos used by the player
	 */
	public abstract void resetUndosUsedPerPlayer();
	
	/**
	 * increments the number of undos used by a given player
	 * @param player - the player using the undo
	 */
	public abstract void incrementUndosUsedByPlayer(Player player);
	
	/**
	 * @param player - the given player
	 * @return the number of undos used by the given player
	 */
	public abstract Integer getUndosUsedByPlayer(Player player);

	/**
	 * checks if the players hand is empty
	 * @return true if hand is empty
	 */
	public abstract boolean checkPlayerHandsAreEmpty();
	
	/**
	 * add an observer
	 * @param o - the observer
	 */
	public abstract void addObserver(Observer o);

}