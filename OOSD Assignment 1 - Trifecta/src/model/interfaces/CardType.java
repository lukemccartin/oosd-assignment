package model.interfaces;

import java.util.HashMap;

/**
 * Interface representing a card's type.
 * 
 * @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */
public interface CardType {

	/**
	 * @param set the card's id
	 */
	public abstract void setId(String id);
	
	/**
	 * @return the card's id
	 */
	public abstract String getId();
	
	/**
	 * @return the card's placeability
	 */
	public abstract CardPlaceability getCardPlaceability();
	
	/**
	 * @param cardPlaceability - set the card's placeability
	 */
	public abstract void setPlaceability(CardPlaceability cardPlaceability);
	
	/**
	 * @return the card's giveability
	 */
	public abstract CardGiveability getCardGiveability();
	
	/**
	 * @param cardGiveability - set the card's giveability
	 */
	public abstract void setGiveability(CardGiveability cardGiveability);
	
	/**
	 * get the current rotation of the card
	 */
	public abstract double getCurrentRotationAxis();

	/**
	 * @return the edges of the card
	 */
	public abstract HashMap<String, Boolean> getEdges();
	
	/**
	 * @param edges - a map of each edge and it's accessibility
	 */
	public abstract void setEdges(HashMap<String, Boolean> edges);

	/**
	 * @param edge - the edge/side of the card
	 * @return if the edge is accessible (open/closed)
	 */
	public abstract boolean isEdgeAccessible(String edge);
	
	/**
	 * rotates the card 90 degrees clockwise
	 */
	public abstract void rotateCard90Degrees();

}
