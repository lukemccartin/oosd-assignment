package model.interfaces;

import model.cardcollection.CardStack;

/**
 * Interface representing a node in a path
 * 
 * @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */
public interface PathNode{
	
	/**
	 * returns the coordinate of the node
	 * @return
	 */
	public int[] getCoordinate();
	
	/**
	 * returns the card stack associated with the node
	 * @return
	 */
	public CardStack getSquare();
}
