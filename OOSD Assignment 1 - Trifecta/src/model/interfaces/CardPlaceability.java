package model.interfaces;

import model.cardcollection.CardStack;
import model.exception.PlaceabilityException;

/**
 * Interface representing a card's placeability on the board
 * 
 * @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */
public interface CardPlaceability {
	
	/**
	 * attempt to place card on the CardStack of a square
	 * 
	 * @param cardStack - the CardStack to place the card on
	 * @param card 
	 * @return true if successful
	 */
	public abstract boolean placeCardOnSquare(CardStack cardStack, Card card, int row ,int column) throws PlaceabilityException;
	
}
