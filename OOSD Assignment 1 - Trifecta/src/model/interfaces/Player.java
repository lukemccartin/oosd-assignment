package model.interfaces;

import java.util.List;

/**
 * Interface representing the player.
 * 
 * @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */
public interface Player {
	
	/**
	 * @return player id
	 */
	public abstract int getPlayerId();
	
	/**
	 * @param playerId - set the player's id
	 */
	public abstract void setPlayerId(int playerId);
	
	/**
	 * @return the player's score
	 */
	public abstract int getPlayerScore(); 
	
	/**
	 * @param points - set the player's score by adding points to the existing score
	 */
	public abstract void setPlayerScore(int points); 
	
	/**
	 * @return the player's hand as a list of cards
	 */
	public abstract List<Card> getPlayerHand(); 
		
	/**
	 * @return the player's type
	 */
	public abstract PlayerType getPlayerType();
	
	/**
	 * @param playerType - set the player's type
	 * 	
	 */
	public abstract void setPlayerType(PlayerType playerType);
	
}
