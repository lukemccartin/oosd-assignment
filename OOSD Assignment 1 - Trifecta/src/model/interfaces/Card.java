package model.interfaces;

/**
 * Interface representing a card.
 * 
 * @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */
public interface Card {
		
	/**
	 * @return - the card's type
	 */
	public abstract CardType getCardType();
	
	/**
	 * @param cardType - set the card's type
	 */
	public abstract void setCardType(CardType cardType);
	
	/**
	 * @return true if card is facing down
	 */
	public abstract boolean getIsCardFacingDown();
	
	/**
	 * @param isCardFacingDown - true if card is facing down
	 */
	public abstract void setIsCardFacingDown(boolean isCardFacingDown);
}
