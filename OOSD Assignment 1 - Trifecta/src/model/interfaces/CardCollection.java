package model.interfaces;

import java.util.Collection;

/**
 * Interface representing a collection of cards.
 * 
 * @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */
public interface CardCollection {
	
	/**
	 * add a card to the collection
	 * 
	 * @param card - the card to be added
	 * @return true if the card was added successfully
	 */
	public abstract boolean addCard(Card card);
	
	/**
	 * @return the collection of cards
	 */
	public abstract Collection<Card> getCards();

}
