package model.interfaces;

import java.util.HashMap;

import model.exception.PlaceabilityException;

/**
 * Interface representing the player type
 * 
 * @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */
public interface PlayerType {
	
	/**
	 * checks a player to see if a decorated effect is applied
	 * @throws PlaceabilityException
	 */
	public abstract void checkForDecoratedEffect() throws PlaceabilityException;
	
	/**
	 * @return a list of effects that are applied to a player
	 */
	public abstract HashMap<String, String> getListOfEffectsAppliedToPlayer();
	
	/**
	 * adds an effect to a player
	 * @param playerString - the effect to be applied
	 * @param message
	 */
	public abstract void addEffect(String playerString, String message);

	/**
	 * removes an effect from the player
	 * @param playerTypeId - the id of the effect to be removed
	 */
	public abstract void removeEffect(String playerTypeId);
	
	/**
	 * @return the base player type
	 */
	public abstract PlayerType getBasePlayerType();

}
