package model.interfaces;

/**
 * Interface containing constants used in the game.
 * 
 * @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */
public interface GameConstants {
	
	public static final int NUM_ROUNDS = 3;
	
	public static final int SMALL_BOARD = 6;
	
	public static final int MEDIUM_BOARD = 8;
	
	public static final int LARGE_BOARD = 10;
	
	public static final int DECK_SIZE_MODIFIER = 9; 
	
	public static final int RATIO_OF_PATH_CARDS_TO_NON_PATH_CARDS = 6;
	
	public static final int PLAYER_HAND_SIZE = 5;
	
	public static final String TRANSITION_ROUNDS = "transition";
	
	public static final String BOARD_RESET = "resetBoardSquareRoations";

	public static final String END_OF_GAME = "endOfGame";
	
	public enum PlayablePathCardType {
		tIntersectPathCard,
		straightPathCard,
		rightTurnPathCard,
		leftTurnPathCard,
		fourWayIntersectPathCard,
		deadEndPathCard
	}
	
	public enum GoalCardType {
		falseGoalPathCard,
		trueGoalPathCard
	}
	
	public enum StartCardType {
		startPathCard
	}
	
	public enum ActionCardType {
		clearPathActionCard,
		goalRevealActionCard,
		landfillActionCard,
		landfillRemovalActionCard,
		logBlockActionCard,
		logBlockRemovalActionCard
	}
	
	public enum PersonalCardType {
		chippedToothPersonalCard,
		hurtPawPersonalCard,
		healedToothPersonalCard,
		healedPawPersonalCard,
		skipTurnPersonalCard,
		immunityPersonalCard
	}
	
}
