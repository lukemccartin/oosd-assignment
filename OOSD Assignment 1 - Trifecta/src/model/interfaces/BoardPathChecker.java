package model.interfaces;

import java.util.List;
import model.interfaces.PathNode;

/**
 * Interface representing the board path checker which checks if paths from the start 
 * to the goal are clear of obstacles and complete
 * 
 * @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */
public interface BoardPathChecker {
	
	 /**
	  * for a given node in the path, return the neighbouring accessible nodes in the path
	  * @param pathNode - a node in the path
	  * @return neighbouringSuccessors - a list of neighbouring nodes which are accessible from the given path node 
	  */
	public abstract List<PathNode> getNeighbouringSuccessors(PathNode pathNode);
	
	/**
	 * Performs depth first search algorithm by traversing each path node (which is a square with a path card on top of its stack)
	 * on the board.
	 * 
	 * @param startCoordinate - the coordinate of the start path node on the board
	 * @param GoalCoordinate - the coordinate of the goal path node on the board
	 * @return true if a path from the start to the goal is able to be found.
	 */
	public abstract boolean depthFirstSearch(int[] startCoordinate, int[] GoalCoordinate);
	
}
