package model.interfaces;

import java.util.Observer;

/**
 * Interface representing the round manager
 * 
 * @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */
public interface RoundManager {
	
	/**
	 * assign the player types, call the deck to shuffle itself, call the board to set itself up 
	 * for a new round, call the dealer to deal a new set of cards to the players 
	 * and call the turn manager to start the first turn
	 */
	public abstract void startRound();
	
	/**
	 * call the board to clear itself, take each card out of the discard pile reset
	 * the cards rotation and put it back in the deck and notify observers of end of round or
	 * end of game
	 */
	public abstract void endRound();
	
	/**
	 * @return the current round
	 */
	public abstract int getCurrentRound();
	
	/**
	 * add an observer
	 * @param o - the observer
	 */
	public abstract void addObserver(Observer o);

}
