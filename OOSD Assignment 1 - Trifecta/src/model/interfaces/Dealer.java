package model.interfaces;

/**
 * Interface representing the dealer for the game.
 * 
 * @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */
public interface Dealer {

	/**
	 * deal cards to each of the players in the game one by one until their
	 * player hand is equal to the specified player hand size in the
	 * GameConstants class
	 */
	public abstract void dealCardsToPlayersAtStartOfRound();

	/**
	 * remove a card from the deck and add it to the player's hand
	 * 
	 * @param player - the player to deal a card to
	 */
	public abstract void dealCardToPlayer(Player player);

}