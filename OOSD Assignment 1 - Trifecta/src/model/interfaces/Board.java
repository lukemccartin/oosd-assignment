package model.interfaces;

import java.util.Observer;

import model.cardcollection.CardStack;
import model.exception.CardMovementException;

/**
 * Interface representing the board.
 * 
 * @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */
public interface Board {
	
	/**
	 * for each row and column on the board create a new CardStack
	 */
	public abstract void createSquaresOnBoard(int boardSize);
	
	/**
	 * called from the GameEngine at the start of a round to set up the board
	 * by creating the squares on the board, setting the start square and 
	 * setting the goal squares
	 */
	public abstract void setUpBoard(int numberOfGoals);
	
	/**
	 * clear the board
	 */
	public abstract void clearBoard();
	
	/**
	 * @return the board size
	 */
	public abstract int getBoardSize();
	
	/**
	 * for a given row and column return the CardStack on that square
	 * 
	 * @param row - the row of the square
	 * @param column - the column of the square
	 * @return the CardStack on the square
	 */
	public abstract CardStack getSquare(int row, int column);
	
	/**
	 * for a given row and column add a card to the CardStack on that square
	 * 
	 * @param row - the row of the square
	 * @param column - the column of the square
	 * @param card - the card to be added to the square
	 * @return true if the card was added to the CardStack successfully
	 */
	public abstract boolean addCardToSquare(int row, int column, Card card);
		
	/**
	 * for a given row and column remove a card from the CardStack on that square
	 * 
	 * @param row - the row of the square
	 * @param column - the column of the square
	 * @param card - the card to be removed from the square
	 */
	public abstract void removeCardFromSquare(int row, int column, Card card);
	
	/**
	 * handles movement of the goal card before a round starts
	 * @param row - the row coordinate on the board
	 * @param col - the column coordinate on the board
	 * @throws CardMovementException
	 */
	public abstract void handleGoalCardMovement(int row, int col) throws CardMovementException;
	
	/**
	 * sets the coordinates of the moved goal
	 * @param row - the row coordinate on the board
	 * @param col - the column coordinate on the board
	 */
	public abstract void setGoalCardMovementCoordinates(int row, int col);
	
	/**
	 * adds an observer
	 * @param o - the observer
	 */
	public abstract void addObserver(Observer o);

	/**
	 * flips a card given the coordinate on the board
	 * @param row - the row coordinate on the board
	 * @param column - the column coordinate on the board
	 */
	public abstract void flipCard(int row, int column);

	/**
	 * @return the coodrinate of the start card
	 */
	public abstract int[] getStartCoordinate();

	/**
	 * @return the coordinate of the true goal card
	 */
	public abstract int[] getTrueGoalCoordinate();
	
	/**
	 * notifies observers of start and goal locations
	 */
	public abstract void updateGameBoardWithStartAndGoalLocations();

	/**
	 * checks if any of the neighbouring cards need to be flipped
	 * @param row - the row coordinate on the board
	 * @param column - the column coordinate on the board
	 */
	public abstract void checkNeighboursToFlip(int row, int column);

	


	
}
