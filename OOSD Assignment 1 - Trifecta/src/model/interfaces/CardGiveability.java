package model.interfaces;

import model.exception.GiveabilityException;

/**
 * Interface representing a card's giveability to a player
 * 
 * @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */
public interface CardGiveability {

	/**
	 * attempt to give a card to a player
	 * 
	 * @param card - the card being given to another player
	 * @param player - the player being given the card
	 */
	public abstract void giveCardToPlayer(Player player, Card card) throws GiveabilityException;
	
}
