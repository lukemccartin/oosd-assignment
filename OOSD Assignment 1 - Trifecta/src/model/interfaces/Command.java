package model.interfaces;


/**
 * Interface representing a Command.
 * 
 * @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */
public interface Command {
	
	/**
	 * calls a method in the receiver (TurnManager) to perform the operation of the command
	 * 
	 * @return true if the command was successfully executed
	 */
	public abstract boolean execute();
	
	/**
	 * calls a method in the receiver (TurnManager) to reverse the operation of the command
	 */
	public abstract void undo();

}
