package model.interfaces;

import model.cardcollection.CardStack;

/**
 * Interface representing the game engine which sets up the game, starts a new round,
 * and stores the number of the current round.
 * 
 * @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */
public interface GameEngine  {
			
	/**
	 * Create the discard pile, pass the number of players to the player manager and
	 * pass the time limit for each turn to the turn manager
	 * 
	 * @param numPlayers - the number of players in the game
	 * @param timeLimitForEachTurn - the time limit for each turn
	 */
	public abstract void setUpGame(int numPlayers, int timeLimitForEachTurn, int boardSize, int numberOfGoals);
	
	/**
	 * @return the discard pile
	 */
	public abstract CardStack getDiscardPile();
	
	/**
	 * @return the number of players
	 */
	public abstract int getNumPlayers();
	
}
