package model.interfaces;


/**
 * Interface representing the command manager which stores, executes and undo's command. The invoker in the
 * command pattern.
 * 
 * @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */
public interface CommandManager {
	
	/**
	 * calls the execute method of a command and if it is able to 
	 * be executed it then stores the command in a linked list.
	 * 
	 * @param player - the player to deal a card to
	 */
	public abstract void storeAndExecute(Command command);
	
	/**
	 * for a rounds worth of commands, remove the last command in the linked list
	 * and call its undo method
	 */
	public abstract void undoLastRoundOfCommands();
	
	/**
	 * clear the linked list of commands
	 */
	public abstract void clearCommands();

}
