package model.interfaces;

import java.util.ArrayList;

import model.interfaces.PathNode;

/**
 * Interface representing the board square neighbour checker which checks neighbouring cards to see if they are 
 * goal type cards, and checks if any of the neighbouring cards are accessible.
 * 
 * @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */
public interface BoardSquareNeighbourChecker {

	/**
	 * for a given path card which a player is attempting to place on a certain position on the board, 
	 * returns true if at least one edge of the path displayed on the card can be matched to a neighbouring 
	 * card already on the board with an open path
	 * @param card - the path card that the player is attempting to place on the board
	 * @param row - the row coordinate on the board where the player is attempting to place the card
	 * @param column - the column coordinate on the board where the player is attempting to place the card
	 * @return neighbourAccessible - true if at least one of the neighbouring coordinates on the board has
	 * a path card which has a matching open path to the card being placed 
	 */
	public abstract boolean checkIfNeighbourIsAccessible(Card card, int row, int column);
	
	/**
	 * for a given node in the path, returns a list of accessible neighbouring path nodes
	 * @param pathNode - the node in the path
	 * @return accessibleNeighbours - a list of nodes in the path which can be accessed from the given node
	 */
	public abstract ArrayList<PathNode> getAccessibleNeighbours(PathNode pathNode);

	/**
	 * for a given path card which a player is attempting to place on a position on the board, checks if any of
	 * the neighbouring cards on the board are goal type cards
	 * @param card - the path card that the player is attempting to place on the board
	 * @param row - the row coordinate on the board where the player is attempting to place the card
	 * @param column - the column coordinate on the board where the player is attempting to place the card
	 * @return isGoal - true if any of the neighbouring cards on the board are goal type cards regardless of if the
	 * path on the card and the path on the neighbouring card are accessible
	 */
	public abstract boolean checkIfAnyNeighbourIsGoalTypeCard(Card card, int row, int column);

	/**
	 * for a given path card which a player is attempting to place on a position on the board next to a goal type card, 
	 * checks if either the card will be attached adjacent to a pre-existing and accessible path or if the goal type card 
	 * neighbours are attached to pre-existing paths, and returns whether the card is placeable
	 * @param card - the path card that the player is attempting to place on the board
	 * @param row - the row coordinate on the board where the player is attempting to place the card
	 * @param column - the column coordinate on the board where the player is attempting to place the card
	 * @return placeable - true if the position on the board has an adjacent, pre-existing, accessible path, or if the adjacent 
	 * goal cards are attached to pre-existing paths
	 */
	public abstract boolean checkIfCardCanBePlacedNextToGoalCard(Card card, int row, int column);

}
