package model.interfaces;

/**
 * Interface representing the deck.
 * 
 * @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */
public interface Deck {

	/**
	 * create cards and add them to the deck based on the ratio of path cards to action cards required
	 */
	public abstract void populateDeck();
	
	/**
	 * shuffle the cards in the deck
	 */
	public abstract void shuffleDeck();

}
