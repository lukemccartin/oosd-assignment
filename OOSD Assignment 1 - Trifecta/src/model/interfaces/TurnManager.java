package model.interfaces;

import java.util.List;
import java.util.Observer;

import model.TurnTimerService;
import model.cardcollection.CardStack;

/**
 * Interface representing the turn manager which handles each turn by setting the current player,
 * reseting the timer at the end of each turn, storing the card selected by a player during a turn,
 * and handling player actions such as place card on board, discard card and give card to another
 * player.
 * 
 * @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */
public interface TurnManager {
	
	/**
	 * @param timeLimitForEachTurn - set the time limit for each turn
	 */
	public abstract void setTimeLimitForEachTurn(int timeLimitForEachTurn);

	/**
	 * @return the current player
	 */
	public abstract Player getCurrentPlayer();
	
	/**
	 * @return list of all current players
	 */
	public abstract List<Player> getAllPlayers();
	
	/**
	 * @param player - set the current player
	 */
	public abstract void setCurrentPlayer(Player player);
	
	/**
	 * @return the current card selected by the player
	 */
	public abstract Card getCardSelected();
	
	/**
	 * 
	 * @return discardPile
	 */
	public abstract CardStack getDiscardPile();
	/**
	 * @param cardSelected - set the current card selected by the player
	 */
	public abstract void setCardSelected(Card cardSelected);
	
	/**
	 * sets the first player in the list of players to the current player and starts the timer
	 */
	public abstract void startFirstTurn();
	
	/**
	 * resets the timer, sets the next player in the list of players to the current player
	 * and sets the current card selected to null.
	 */
	public abstract void endTurn();
	
	/**
	 * for a given row and column check if the card selected by the player can be added to the 
	 * CardStack on that square.
	 * If card can be added call the player to remove the card from the player's hand, call the 
	 * board to add it to the square, call the dealer to deal the player a new card from the deck 
	 * and then end the turn
	 * 
	 * @param row - the row of the square
	 * @param column - the column of the square
	 * @return true if the card was able to be placed on the square
	 */
	public abstract boolean placeCardOnBoard(int row, int column);
	
	/**
	 * undo's placing a card on the board by removing the card last added to the player's hand and removing the 
	 * card last added to a given square on the board and adding it back to the player's hand.
	 * 
	 * @param row - the row of the square
	 * @param column - the column of the square
	 * @param player - the current player for the undo operation
	 * 
	 */
	public abstract void undoPlaceCardOnBoard(int row, int column, Player player);
		
	/**
	 * removes the card selected by the player from the player's hand and adds it to the discard pile, 
	 * calls the dealer to deal the player a new card and then calls end turn.
	 */
	public abstract boolean playerDiscardCard();
	
	/**
	 * @return the TurnTimerService
	 */
	public abstract TurnTimerService getTurnTimerService();
	
	/**
	 * start the timer
	 */
	public abstract void setTurnTimer();
	
	/**
	 * handles canceling the current turn timer service
	 */
	public abstract void cancelCurrentTurnTimer();
	
	/**
	 * Called if the timer runs out. Removes the last card from the player's hand and adds it to the discard pile, 
	 * calls the dealer to deal the player a new card and then calls end turn.
	 */
	public abstract boolean forcedDiscard();
	
	/**
	 * undos the discard of a card by a player by removing the card last added to the player's hand and removing the 
	 * card last added to the discard pile and adding it back to the player's hand.
	 * 
	 * @param player - the current player for the undo operation
	 */
	public abstract void undoDiscardCard(Player player);
	
	/**
	 * Handles the applying of personal card effects to players
	 * @param playerId - the playerId number whom the current player is attempting to place a personal card on
	 * @return true if card was able to be given to player
	 */
	public abstract boolean giveCardToPlayer(int playerId);
	
	/**
	 * undos a card given from one player to another by removing the last card added from the players
	 * hand giving the card, removing the top card from the discard pile and adding it back to the from player's hand
	 * and removing the decoration from the player who was given the card
	 * 
	 * @param fromPlayer - the player giving the card to another
	 * @param toPlayerId - the id of the player receiving the card
	 */
	public abstract void undoGiveCardToPlayer(Player fromPlayer, int toPlayerId);
	
	/**
	 * called by concrete decorator classes that auto remove themselves as per their implementation
	 * @param toPlayer - the player that the associated removal is performed on
	 */
	public abstract void notifyForPaneChange(Player toPlayer);
	
	public abstract void addObserver(Observer o);
		
}