package model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import model.interfaces.Card;
import model.interfaces.PathNode;
import model.card.actioncard.AbstractActionCard;
import model.interfaces.BoardSquareNeighbourChecker;

/**
 * Class representing the board square neighbour checker which checks neighbouring cards to see if they are 
 * goal type cards, and checks if any of the neighbouring cards are accessible.
 * 
 * @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */
public class BoardSquareNeighbourCheckerImpl implements BoardSquareNeighbourChecker {
	
	private static BoardSquareNeighbourChecker singletonInstance;

	
	private BoardSquareNeighbourCheckerImpl(){
		
	}
	
	
	public static BoardSquareNeighbourChecker getSingletonInstance(){
		if (singletonInstance == null){
			singletonInstance = new BoardSquareNeighbourCheckerImpl();
		}
		return singletonInstance;
	}
	
	
	@Override
	public boolean checkIfAnyNeighbourIsGoalTypeCard(Card card, int row, int column) {
		boolean isGoal = false;
		HashMap<String, Boolean> neighbouringEdges = getNeighbouringEdges(row, column);

		for (String edge: neighbouringEdges.keySet()){
			Card neighbouringCard = getNeighbouringPathNode(edge, row, column).getSquare().getCards().peek();
			if(neighbouringCard.getCardType().getId() == "trueGoalPathCard"){
				isGoal = true;
			}
			else if (neighbouringCard.getCardType().getId() == "falseGoalPathCard"){
				isGoal = true;
			}
		}
			return isGoal;
	}

	
	@Override
	public boolean checkIfNeighbourIsAccessible(Card card, int row, int column) {
		boolean neighbourAccessible = false;
		int matchCount = 0;
		HashMap<String, Boolean> neighbouringEdges = getNeighbouringEdges(row, column);
		
		for (String edge: neighbouringEdges.keySet()){
			if(card.getCardType().getEdges().get(edge) && neighbouringEdges.get(edge)){
				matchCount++;
			}
		}
		if (matchCount > 0){
			neighbourAccessible = true;
		}
		return neighbourAccessible;
	}


	
	@Override
	public ArrayList<PathNode> getAccessibleNeighbours(PathNode pathNode){
		int row = pathNode.getCoordinate()[0];
		int column = pathNode.getCoordinate()[1];
		ArrayList<PathNode> accessibleNeighbours = new ArrayList<PathNode>();
		HashMap<String, Boolean> pathNodeEdges = pathNode.getSquare().getCards().peek().getCardType().getEdges();
		HashMap<String, Boolean> neighbouringEdges = getNeighbouringEdges(row, column);

		for(String string:neighbouringEdges.keySet()){
			if(pathNodeEdges.get(string) == neighbouringEdges.get(string)){
				if(neighbouringEdges.get(string)){
					PathNode newPathNode = getNeighbouringPathNode(string, row, column);
					accessibleNeighbours.add(newPathNode);
				}
			}
		}
		return accessibleNeighbours;
	}

	
	@Override
	public boolean checkIfCardCanBePlacedNextToGoalCard(Card card, int row, int column) {
		boolean placeable = false;
		boolean accessible = checkIfNeighbourIsAccessible(card, row, column);
		int connectedPathCardCount = 0;
		HashMap<String, Boolean> neighbouringEdges = getNeighbouringEdges(row, column);
		List<String> edgesToFlip = new ArrayList<String>();
		
		for (String edge: neighbouringEdges.keySet()){
			if(card.getCardType().getEdges().get(edge)){
				Card neighbouringCard = getNeighbouringPathNode(edge, row, column).getSquare().getCards().peek();
				if(neighbouringCard != null){
					if((neighbouringCard.getCardType().getId() == "trueGoalPathCard" || neighbouringCard.getCardType().getId() == "falseGoalPathCard")){
						edgesToFlip.add(edge);
						//check if neighbouring goal card is connected to a path
						if(checkIfNeighbouringGoalCardHasPathNeighbours(neighbouringCard, edge, row, column)){
							connectedPathCardCount++;
						}
					}
					else if (!(neighbouringCard.getCardType() instanceof AbstractActionCard)){
						connectedPathCardCount++;
					}
				}
			}
		}
		if(accessible && connectedPathCardCount > 0){
			for(String edge:edgesToFlip){
				flipCard(edge,row,column);
			}
			placeable = true;
		}
		return placeable;
	}
	
	/**
	 * given a set of coordinates, creates a hash map that contains the accessibility of any existing 
	 * neighbouring cards on the board
	 * @param row - the row coordinate on the board
	 * @param column - the column coordinate on the board
	 * @return neighbouringEdges - a hash map of the accessibility of the neighbouring cards given a set
	 * of coordinates
	 */
	private HashMap<String, Boolean> getNeighbouringEdges(int row, int column) {
		int gridSize = BoardImpl.getSingletonInstance().getBoardSize();
		HashMap<String, Boolean> neighbouringEdges = new HashMap<String, Boolean>();

		if (row > 0 && !BoardImpl.getSingletonInstance().getSquare(row-1, column).getCards().isEmpty()){
			Card topEdgeNeighbour = BoardImpl.getSingletonInstance().getSquare(row-1, column).getCards().peek();
			neighbouringEdges.put("top", topEdgeNeighbour.getCardType().isEdgeAccessible("bottom"));
		}
		if (row < gridSize-1 && !BoardImpl.getSingletonInstance().getSquare(row+1, column).getCards().isEmpty()){
			Card bottomEdgeNeighbour = BoardImpl.getSingletonInstance().getSquare(row+1, column).getCards().peek();
			neighbouringEdges.put("bottom", bottomEdgeNeighbour.getCardType().isEdgeAccessible("top"));
		}
		if(column > 0 && !BoardImpl.getSingletonInstance().getSquare(row, column-1).getCards().isEmpty()){
			Card leftEdgeNeighbour = BoardImpl.getSingletonInstance().getSquare(row, column-1).getCards().peek();
			neighbouringEdges.put("left", leftEdgeNeighbour.getCardType().isEdgeAccessible("right"));
		}
		if(column < gridSize-1 && !BoardImpl.getSingletonInstance().getSquare(row, column+1).getCards().isEmpty()){
			Card rightEdgeNeighbour = BoardImpl.getSingletonInstance().getSquare(row, column+1).getCards().peek();
			neighbouringEdges.put("right", rightEdgeNeighbour.getCardType().isEdgeAccessible("left"));
		}
		return neighbouringEdges;
		
	}

	/**
	 * given an edge position of a card and a set of coordinates on the board, return the neighbouring nodes in existing paths 
	 * adjacent to the edge position
	 * @param edgeName - identifies which edge of the card is to be evaluated
	 * @param row - the row coordinate on the board
	 * @param column - the column coordinate on the board
	 * @return neighbouringPathNode - returns the neighbouring node in the path
	 */
	private PathNode getNeighbouringPathNode(String edgeName, int row, int column) {
		PathNode neighbouringPathNode = null;
		
		if(edgeName == "top" && BoardImpl.getSingletonInstance().getSquare(row-1, column) != null){	
			neighbouringPathNode = new PathNodeImpl(BoardImpl.getSingletonInstance().getSquare(row-1, column), row-1, column);
			row=row-1;		
		}
		else if (edgeName == "bottom" && BoardImpl.getSingletonInstance().getSquare(row+1, column) != null){
			neighbouringPathNode = new PathNodeImpl(BoardImpl.getSingletonInstance().getSquare(row+1, column), row+1, column);
			row = row+1;
		}
		else if (edgeName == "left" && BoardImpl.getSingletonInstance().getSquare(row, column-1) != null){
			neighbouringPathNode = new PathNodeImpl(BoardImpl.getSingletonInstance().getSquare(row, column-1), row, column-1);
			column = column-1;
		}
		else if (edgeName == "right" && BoardImpl.getSingletonInstance().getSquare(row, column+1) != null){
			neighbouringPathNode = new PathNodeImpl(BoardImpl.getSingletonInstance().getSquare(row, column+1), row, column+1);
			column = column+1;
		}
		return neighbouringPathNode;
	}

	/**
	 * given an edge position of a card and a set of coordinates on the board, flip the neighbouring cards and any
	 * connected cards that need to be flipped
	 * @param edge - identifies which edge of the card is to be evaluated
	 * @param row - the row coordinate on the board
	 * @param column - the column coordinate on the board
	 */
	private void flipCard(String edge, int row, int column) {
		
		if(edge == "top"){	
			BoardImpl.getSingletonInstance().flipCard(row-1, column);
			BoardImpl.getSingletonInstance().checkNeighboursToFlip(row-1, column);
		}
		else if (edge == "bottom"){
			BoardImpl.getSingletonInstance().flipCard(row+1, column);
			BoardImpl.getSingletonInstance().checkNeighboursToFlip(row+1, column);
		}
		else if (edge == "left"){
			BoardImpl.getSingletonInstance().flipCard(row, column-1);
			BoardImpl.getSingletonInstance().checkNeighboursToFlip(row, column-1);
		}
		else if (edge == "right"){
			BoardImpl.getSingletonInstance().flipCard(row, column+1);
			BoardImpl.getSingletonInstance().checkNeighboursToFlip(row, column+1);
		}
		
	}

	/**
	 * given a goal type card on the board, evaluate whether the goal type card has any pre-existing connected paths 
	 * @param card - the goal card to be evaluated
	 * @param edgeName - the direction of the goal card in relation to the card that the player is attempting to place on the board
	 * @param row - the row coordinates of the card the player is attempting to place on the board
	 * @param column - the column coordinates of the card the player is attempting to place on the board
	 * @return hasPathNeighbours - returns true if the goal type card has pre-existing connected paths
	 */
	private boolean checkIfNeighbouringGoalCardHasPathNeighbours(Card card, String edgeName, int row, int column) {
		boolean hasPathNeighbours = false;
		HashMap<String, Boolean> neighbouringEdges = null;

		//set the neighbouring edges of the goal type card
		if(edgeName == "top"){	
			neighbouringEdges = getNeighbouringEdges(row-1, column);
			row = row-1;
		}
		else if (edgeName == "bottom"){
			neighbouringEdges = getNeighbouringEdges(row+1, column);
			row = row+1;
		}
		else if (edgeName == "left"){
			neighbouringEdges = getNeighbouringEdges(row, column-1);
			column = column-1;
		}
		else if (edgeName == "right"){
			neighbouringEdges = getNeighbouringEdges(row, column+1);
			column = column+1;
		}
		
		//check goal type card's neighbours
		for (String edge: neighbouringEdges.keySet()){
			
			if(card.getCardType().getEdges().get(edge)){
				Card neighbouringCard = getNeighbouringPathNode(edge, row, column).getSquare().getCards().peek();
				if(neighbouringCard != null){
					//if the goal type card has neighbours that aren't goal cards then must be connected to an existing path
					if(neighbouringCard.getCardType().getId() != "trueGoalPathCard" && neighbouringCard.getCardType().getId() != "falseGoalPathCard"){
						hasPathNeighbours = true;
					}
					//if the goal type card has neighbours that are not facing down then must be connected to an existing path
					if(!neighbouringCard.getIsCardFacingDown()){
						hasPathNeighbours = true;
					}
				}
			}
		}
		return hasPathNeighbours;
	}
	

}