package model;

import java.util.LinkedList;

import model.interfaces.Command;
import model.interfaces.CommandManager;

/**
 * Class representing the command manager which stores, executes and undo's command. The invoker in the
 * command pattern.
 * 
 * @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */
public class CommandManagerImpl implements CommandManager{

	private static CommandManager singletonInstance;
	private LinkedList<Command> commands;
	
	private CommandManagerImpl() {
		commands = new LinkedList<Command>();
	}

 	public static CommandManager getSingletonInstance(){
 	    if(singletonInstance == null)
            singletonInstance = new CommandManagerImpl();

        return singletonInstance;
 	}

	@Override
	public void storeAndExecute(Command command) {
		if(command.execute())
			commands.add(command);
	}

	@Override
	public void undoLastRoundOfCommands(){
		for(int i=0; i < GameEngineImpl.getSingletonInstance().getNumPlayers(); i++){
			if(!commands.isEmpty()){
				Command command = commands.removeLast();
				command.undo();
			}
		}
	}
	
	@Override
	public void clearCommands(){
		commands.clear();
	}
	
}
