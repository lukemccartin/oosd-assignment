package model;

import model.cardcollection.CardStack;
import model.interfaces.Board;
import model.interfaces.GameEngine;
import model.interfaces.PlayerManager;
import model.interfaces.TurnManager;

/**
 * Class representing the game engine which sets up the game.
 * 
 * @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */
public class GameEngineImpl implements GameEngine {
	
	private static GameEngine singletonInstance;
	private CardStack discardPile;
	private TurnManager turnManager;
	private PlayerManager playerManager;
	private Board board;
	private int numPlayers;
	
	private GameEngineImpl() {
		
	}
	
 	public static GameEngine getSingletonInstance(){
 	    if(singletonInstance == null)
            singletonInstance = new GameEngineImpl();

        return singletonInstance;
 	}
	
 	@Override
	public void setUpGame(int numPlayers, int timeLimitForEachTurn, int boardSize, int numberOfGoals){

		this.numPlayers = numPlayers;
		discardPile = new CardStack();
		turnManager = TurnManagerImpl.getSingletonInstance();
		playerManager = PlayerManagerImpl.getSingletonInstance();
		playerManager.createPlayers(numPlayers);
		turnManager.setTimeLimitForEachTurn(timeLimitForEachTurn);
		board = BoardImpl.getSingletonInstance();
		board.createSquaresOnBoard(boardSize);
		board.setUpBoard(numberOfGoals);
	}
	

	@Override
	public CardStack getDiscardPile() {
		return discardPile;
	}
	
	@Override
	public int getNumPlayers() {
		return numPlayers;
	}
	
}
