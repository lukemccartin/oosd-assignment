package model;

import java.util.List;

import com.google.java.contract.Ensures;
import com.google.java.contract.Requires;

import model.cardcollection.DeckImpl;
import model.interfaces.Card;
import model.interfaces.Dealer;
import model.interfaces.GameConstants;
import model.interfaces.Player;

/**
 * Class representing the dealer for the game.
 * 
 * @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */
public class DealerImpl implements Dealer {

	private static Dealer singletonInstance;
	
	private DealerImpl() {
	
	}
	
 	public static Dealer getSingletonInstance(){
 	    if(singletonInstance == null)
            singletonInstance = new DealerImpl();

        return singletonInstance;
 	}

	@Override
	public void dealCardsToPlayersAtStartOfRound(){
		List<Player> players = PlayerManagerImpl.getSingletonInstance().getAllPlayers();
		
		for(int i = 0; i < GameConstants.PLAYER_HAND_SIZE; i++){
			for (Player player : players){
				dealCardToPlayer(player);
			}
		}
	}

	@Override
	@Requires("player.getPlayerHand().size() < GameConstants.PLAYER_HAND_SIZE")
	@Ensures("player.getPlayerHand().size() == old(player.getPlayerHand().size()+ 1)")
	public void dealCardToPlayer(Player player) { 
		if (DeckImpl.getSingletonInstance().getCards().size() > 0){
			Card card = DeckImpl.getSingletonInstance().removeCard();
			player.getPlayerHand().add(card);
			card.setIsCardFacingDown(false);
		}

	}
	
}
