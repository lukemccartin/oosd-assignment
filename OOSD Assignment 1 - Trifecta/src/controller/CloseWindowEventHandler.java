package controller;

import javafx.event.EventHandler;
import javafx.stage.WindowEvent;

/**
 * Class that handles window close event
 * 
 * @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */

public class CloseWindowEventHandler implements EventHandler<WindowEvent>{

	@Override
	public void handle(WindowEvent event) {
		System.exit(0);
	}
}
