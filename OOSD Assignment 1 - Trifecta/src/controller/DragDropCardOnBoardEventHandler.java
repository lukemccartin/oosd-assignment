package controller;

import javafx.event.EventHandler;
import javafx.scene.input.DragEvent;
import model.CommandManagerImpl;
import model.command.PlaceCardOnBoardCommand;
import view.squareview.BoardSquareView;

/**
 * Allows for players to drop cards on board.
 * 
 * @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */

public class DragDropCardOnBoardEventHandler implements EventHandler<DragEvent> {

	public void handle(DragEvent event) {

		BoardSquareView source = (BoardSquareView) event.getSource();
		CommandManagerImpl.getSingletonInstance().storeAndExecute(new PlaceCardOnBoardCommand(source.getRow(), source.getColumn()));
		
		event.setDropCompleted(true);
		event.consume();
	}
}
