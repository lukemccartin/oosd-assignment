package controller;

import javafx.event.EventHandler;
import javafx.scene.input.DragEvent;
import model.CommandManagerImpl;
import model.command.PlayerDiscardCardCommand;

/**
 * Allows cards to be dropped onto discard pile.
 * 
 * @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */

public class DragDropCardOnDiscardPileEventHandler implements EventHandler<DragEvent>{

	@Override
	public void handle(DragEvent event) {

		CommandManagerImpl.getSingletonInstance().storeAndExecute(new PlayerDiscardCardCommand());
		
		event.setDropCompleted(true);
		event.consume();
	}

}
