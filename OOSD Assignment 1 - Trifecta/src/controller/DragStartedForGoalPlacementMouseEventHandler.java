package controller;

import javafx.event.EventHandler;
import javafx.scene.image.Image;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import model.BoardImpl;
import model.cardcollection.CardStack;
import model.interfaces.Board;
import view.squareview.BoardSquareView;

/**
 * Handles goals picked up for movement during goal placement.
 * 
 * @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */
public class DragStartedForGoalPlacementMouseEventHandler implements EventHandler<MouseEvent> {

	public void handle(MouseEvent event) {
		
		Board board = BoardImpl.getSingletonInstance();
		BoardSquareView source = (BoardSquareView) event.getSource();
		int row = source.getRow();
		int col = source.getColumn();
		
		int[] startCoords = board.getStartCoordinate();
		CardStack movementCoordStack = board.getSquare(row, col);
		
		//Ensures start path card cannot be moved, only allowing goals to be moved
		if(!(startCoords[0] == row && startCoords[1] == col) && !(movementCoordStack.getCards().isEmpty())){
			board.setGoalCardMovementCoordinates(row, col);
			
			Dragboard db = source.startDragAndDrop(TransferMode.ANY);
			ClipboardContent content = new ClipboardContent();
			Image dragView = source.snapshot(null, null);
			db.setDragView(dragView, 20, 20);
			
			content.putImage(dragView);
			db.setContent(content);
			event.consume();
		}
	}
}
