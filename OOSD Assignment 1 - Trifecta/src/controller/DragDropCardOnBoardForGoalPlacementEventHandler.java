package controller;

import javafx.event.EventHandler;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.input.DragEvent;
import model.BoardImpl;
import model.exception.CardMovementException;
import model.interfaces.Board;
import view.squareview.BoardSquareView;
import view.utilities.AlertDialog;

/**
 * Handles the movement of goal cards for players before rounds.
 * 
 * @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */

public class DragDropCardOnBoardForGoalPlacementEventHandler implements EventHandler<DragEvent> {

	public void handle(DragEvent event) {

		BoardSquareView source = (BoardSquareView) event.getSource();
		Board board = BoardImpl.getSingletonInstance();
		
		int row = source.getRow();
		int col = source.getColumn();
		try {
			board.handleGoalCardMovement(row, col);
		} catch (CardMovementException e) {
			new AlertDialog(AlertType.ERROR , e.getMessage());
		}
		
		event.setDropCompleted(true);
		event.consume();
	}
}