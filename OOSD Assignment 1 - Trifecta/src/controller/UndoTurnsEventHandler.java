package controller;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import model.CommandManagerImpl;
import model.PlayerManagerImpl;
import model.TurnManagerImpl;
import model.interfaces.Player;
import model.interfaces.PlayerManager;
import view.PlayerPane;

/**
 * This class calls the command manager to undo the number of rounds specified by the player
 * and increments the number of undos used counter for that player.
 * 
 * @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */
public class UndoTurnsEventHandler implements EventHandler<ActionEvent>{
	
	private PlayerPane playerPane;
	private PlayerManager playerManager;
	
	public UndoTurnsEventHandler(PlayerPane playerPane) {
		this.playerPane = playerPane;
		playerManager = PlayerManagerImpl.getSingletonInstance();
	}

	@Override
	public void handle(ActionEvent event) {
		
		try{
			
			Player currentPlayer = TurnManagerImpl.getSingletonInstance().getCurrentPlayer();
			Integer numUndosUsedByPlayer = playerManager.getUndosUsedByPlayer(currentPlayer);
			
			//if the player has not used their undo this round yet
			if(numUndosUsedByPlayer < 1){
				
				//undo the number of rounds chosen by the player
				for(int i = 0; i < playerPane.getNumberOfMovesToUndoValue(); i++){
					CommandManagerImpl.getSingletonInstance().undoLastRoundOfCommands();
				}
			
				//increments the number of undos used counter for that player.
				playerManager.incrementUndosUsedByPlayer(currentPlayer);
			}
			
			else
				playerPane.setDialogBoxText("This player has already used undo this round, cannot use it again.");
		}
		catch (NullPointerException npe) {
			playerPane.setDialogBoxText("Required input missing - please select number of moves to undo");
			return;
		}
	}
	
	

}
