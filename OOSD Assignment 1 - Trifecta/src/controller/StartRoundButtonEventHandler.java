package controller;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import model.BoardImpl;
import model.GameEngineImpl;
import model.RoundManagerImpl;
import view.MainAppStage;

/**
 * This class retrieves the values entered by into the StartGamePane once the StartGameButton has been clicked
 * and passes to the MainAppStage to set up the game according to the values entered
 * 
 * @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */
public class StartRoundButtonEventHandler implements EventHandler<ActionEvent> {
	
	private MainAppStage mainAppStage;

	public StartRoundButtonEventHandler(MainAppStage mainAppStage){
		this.mainAppStage = mainAppStage;
	}

	@Override

	public void handle(ActionEvent event) {

		if(RoundManagerImpl.getSingletonInstance().getCurrentRound() == 0) {
			mainAppStage.changeToGameScene(BoardImpl.getSingletonInstance().getBoardSize(), GameEngineImpl.getSingletonInstance().getNumPlayers());
		} else {
			mainAppStage.revertToGameScene();
		}
		
	}
}