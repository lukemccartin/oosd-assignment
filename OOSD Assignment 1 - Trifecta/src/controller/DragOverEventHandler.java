package controller;

import javafx.event.EventHandler;
import javafx.scene.input.DragEvent;
import javafx.scene.input.TransferMode;

/**
 * Allows for SquareView to accept DragAndDrop of Card from player hand.
 * 
 * @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */

public class DragOverEventHandler implements EventHandler<DragEvent> {
	
	@Override
	public void handle(DragEvent event) {
		if (event.getDragboard().hasImage()) {
				event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
			}
		event.consume();	
	}
}
