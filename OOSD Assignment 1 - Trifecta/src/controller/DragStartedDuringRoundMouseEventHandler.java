package controller;

import javafx.event.EventHandler;
import javafx.scene.image.Image;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import model.TurnManagerImpl;
import model.interfaces.Card;
import view.squareview.PlayerHandSquareView;

/**
 * Handles all cards picked up from player hands during rounds.
 * 
 * @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */

public class DragStartedDuringRoundMouseEventHandler implements EventHandler<MouseEvent>{

	public void handle(MouseEvent event) {
		
		PlayerHandSquareView source = (PlayerHandSquareView) event.getSource();
		
		Dragboard db = source.startDragAndDrop(TransferMode.ANY);
		ClipboardContent content = new ClipboardContent();
		
		Card card =  source.getCard();
		
		Image dragView = source.snapshot(null, null);
		db.setDragView(dragView, 20, 20);
		
		TurnManagerImpl.getSingletonInstance().setCardSelected(card);
		content.putImage(dragView);
		db.setContent(content);
		event.consume();
	}
}
