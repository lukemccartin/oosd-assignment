package controller;

import javafx.event.EventHandler;
import javafx.scene.input.DragEvent;
import model.CommandManagerImpl;
import model.command.GiveCardToPlayerCommand;
import view.PlayerEffectsPane;

/**
 * Allows for personal cards to be dropped onto players.
 * 
 * @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */

public class DragDropCardOnIndividualPlayerEventHandler implements EventHandler<DragEvent> {

	@Override
	public void handle(DragEvent event) {
		PlayerEffectsPane playerEffectsPane = (PlayerEffectsPane) event.getSource();
		CommandManagerImpl.getSingletonInstance().storeAndExecute(new GiveCardToPlayerCommand(playerEffectsPane.getAssociatedPlayerId()));
		event.setDropCompleted(true);
		event.consume();
	}

}
