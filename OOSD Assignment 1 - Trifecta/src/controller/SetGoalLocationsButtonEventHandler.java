package controller;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import model.interfaces.GameConstants;
import view.MainAppStage;
import view.StartGamePane;

/**
 * This class retrieves the values entered by into the StartGamePane once the StartGameButton has been clicked
 * and passes to the MainAppStage to set up the game according to the values entered
 * 
 * @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */
public class SetGoalLocationsButtonEventHandler implements EventHandler<ActionEvent> {
	
	private StartGamePane gameSetupPane;
	private int numberOfPlayers;
	private MainAppStage mainAppStage;
	private int timeLimitForEachTurn;
	private int boardSize;
	private int numberOfGoals;
	
	
	public SetGoalLocationsButtonEventHandler(StartGamePane selectNumberOfPlayersPane, MainAppStage mainAppStage){
		this.gameSetupPane = selectNumberOfPlayersPane;
		this.mainAppStage = mainAppStage;
	}

	@Override

	public void handle(ActionEvent event) {

		try {
			//Handles getting all associated selected values by player for game creation
			assert gameSetupPane.getTimeForEachTurnValue() > 0;
			numberOfPlayers = gameSetupPane.getNumberOfPlayersValue();	
			numberOfGoals = gameSetupPane.getNumberOfGoalsValue();
			timeLimitForEachTurn = gameSetupPane.getTimeForEachTurnValue();
			switch(gameSetupPane.getBoardSize()){
				case "Small": boardSize = GameConstants.SMALL_BOARD;
					break;
				case "Medium": boardSize = GameConstants.MEDIUM_BOARD;
					break;
				case "Large": boardSize = GameConstants.LARGE_BOARD;
					break;
				default: boardSize = GameConstants.MEDIUM_BOARD;
					break;
			}
			
		} 
		catch (NullPointerException npe) {
			gameSetupPane.setDialogBoxText("Required input missing - please select game variables");
			return;
		}
		catch (NumberFormatException nfe) {
			gameSetupPane.setDialogBoxText("Invalid input for timer - please enter number greater than 0");
			return;
		}
		catch (AssertionError ae){
			gameSetupPane.setDialogBoxText("Invalid input for timer - please enter number greater than 0");
			return;
		}
		
		mainAppStage.changeToGoalSelectScene(numberOfPlayers, timeLimitForEachTurn, boardSize, numberOfGoals);
	}
}
