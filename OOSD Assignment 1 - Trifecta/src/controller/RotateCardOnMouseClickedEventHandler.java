package controller;

import javafx.animation.RotateTransition;
import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import javafx.util.Duration;
import model.card.pathcard.AbstractPathCard;
import model.interfaces.Card;
import model.interfaces.CardType;
import view.squareview.PlayerHandSquareView;

/**
 * This class retrieves the card from the PlayerHandSquareView that has been clicked and 
 * rotates the card 90 degrees for each mouse click. The card is rotated both in the 
 * model and the view.
 * 
 * @author Elizabeth Shao, Rebecca Laufer, Luke McCartin
 *
 */
public class RotateCardOnMouseClickedEventHandler implements EventHandler<MouseEvent>{

	@Override
	public void handle(MouseEvent mouseEvent) {
		
		PlayerHandSquareView source = (PlayerHandSquareView) mouseEvent.getSource();

		//rotate the card in the model
		Card card = source.getCard();
		CardType cardType = card.getCardType();
		cardType.rotateCard90Degrees();		

		//rotate the card in the view
		if(cardType instanceof AbstractPathCard){
			RotateTransition rt = new RotateTransition(Duration.millis(1), source);
			rt.setByAngle(90);
			rt.play();
		}
		
	}

}
